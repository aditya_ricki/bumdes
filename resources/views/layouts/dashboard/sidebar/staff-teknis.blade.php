<li class="nav-item">
    <a class="nav-link" href="{{ route('staff-teknis.device') }}">
        <i class="ni ni-archive-2 text-green"></i>
        <span class="nav-link-text">Peralatan</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{ route('staff-teknis.packet') }}">
        <i class="ni ni-archive-2 text-green"></i>
        <span class="nav-link-text">Paket Internet</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{ route('staff-teknis.subscribe-candidate') }}">
        <i class="ni ni-archive-2 text-green"></i>
        <span class="nav-link-text">Calon Pelanggan</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{ route('staff-teknis.packet') }}">
        <i class="ni ni-archive-2 text-green"></i>
        <span class="nav-link-text">Pelanggan</span>
    </a>
</li>
