<nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <div class="sidenav-header d-flex align-items-center">
            <a class="navbar-brand" href="/">
                <img src="{{ asset('img/brand/blue.png') }}" class="navbar-brand-img" alt="...">
            </a>
            <div class="ml-auto">
                <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
                    <div class="sidenav-toggler-inner">
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                    </div>
                </div>
            </div>
        </div>
        @php
            $arrayUrl = explode('/', request()->url());
            $currentPath = isset($arrayUrl[4]) ? $arrayUrl[4] : '';
            $prefixKeuangan = ['transaction', 'modal', 'cost', 'report', ''];
            $prefixBlog = ['article-type', 'article', 'selve'];
            $prefixBarang = ['supplier', 'item-type', 'item'];
        @endphp
        <div class="navbar-inner">
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a
                            class="nav-link {{ in_array($currentPath, $prefixKeuangan) ? 'active' : '' }}"
                            href="#navbar-keuangan"
                            data-toggle="collapse"
                            role="button"
                            aria-expanded="true"
                            aria-controls="navbar-keuangan"
                        >
                            <i class="ni ni-money-coins text-success"></i>
                            <span class="nav-link-text">Keuangan</span>
                        </a>
                        <div class="collapse {{ in_array($currentPath, $prefixKeuangan) ? 'show' : '' }}" id="navbar-keuangan">
                            <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="{{ route('dashboard.transaction.selling') }}" class="nav-link">
                                <span class="sidenav-normal"> Transaksi </span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('dashboard.modal') }}" class="nav-link">
                                <span class="sidenav-normal"> Modal </span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('dashboard.cost') }}" class="nav-link">
                                <span class="sidenav-normal"> Cost </span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('dashboard.report') }}" class="nav-link">
                                <span class="sidenav-normal"> Laporan </span>
                                </a>
                            </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ in_array($currentPath, $prefixBlog) ? 'active' : '' }}" href="#navbar-blog" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-blog">
                            <i class="ni ni-single-copy-04 text-primary"></i>
                            <span class="nav-link-text">Blog</span>
                        </a>
                        <div class="collapse {{ in_array($currentPath, $prefixBlog) ? 'show' : '' }}" id="navbar-blog">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item">
                                    <a href="{{ route('dashboard.article-type') }}" class="nav-link">
                                    <span class="sidenav-normal"> Tipe Artikel </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('dashboard.article') }}" class="nav-link">
                                    <span class="sidenav-normal"> Artikel </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('dashboard.selve') }}" class="nav-link">
                                    <span class="sidenav-normal"> Semarmara Blog </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ in_array($currentPath, $prefixBarang) ? 'active' : '' }}" href="#navbar-barang" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-barang">
                            <i class="ni ni-basket text-info"></i>
                            <span class="nav-link-text">Barang</span>
                        </a>
                        <div class="collapse {{ in_array($currentPath, $prefixBarang) ? 'show' : '' }}" id="navbar-barang">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item">
                                    <a href="{{ route('dashboard.suplier') }}" class="nav-link">
                                    <span class="sidenav-normal"> Suplier </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('dashboard.item-type') }}" class="nav-link">
                                    <span class="sidenav-normal"> Tipe Barang </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('dashboard.item') }}" class="nav-link">
                                    <span class="sidenav-normal"> Barang </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('dashboard.letter') }}">
                            <i class="ni ni-books text-red"></i>
                            <span class="nav-link-text">Archive</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('dashboard.user') }}">
                            <i class="ni ni-circle-08 text-black"></i>
                            <span class="nav-link-text">Users</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
