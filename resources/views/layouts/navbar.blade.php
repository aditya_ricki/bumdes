@php
	$selve = App\Services\SelveService::getData();
@endphp
<nav id="navbar-main" class="navbar navbar-horizontal navbar-main navbar-expand-lg navbar-dark bg-primary">
 	<div class="container">
	   <a class="navbar-brand" href="/">
	     	<img src="{{ asset('img/brand/white1.png') }}">
	   </a>
	   <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
	     	<span class="navbar-toggler-icon"></span>
	   </button>
   		<div class="navbar-collapse navbar-custom-collapse collapse" id="navbar-collapse">
		    <div class="navbar-collapse-header">
		       	<div class="row">
		         	<div class="col-6 collapse-brand">
		           		<a href="./pages/dashboards/dashboard.html">
		             		<img src="{{ asset('img/brand/blue.png') }}">
		           		</a>
		         	</div>
		         	<div class="col-6 collapse-close">
		           		<button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
		             		<span></span>
		             		<span></span>
		           		</button>
		         	</div>
		       	</div>
		    </div>
     		<ul class="navbar-nav mr-auto">
		       	<li class="nav-item">
		         	<a href="{{route('blog.home')}}" class="nav-link">
		           		<span class="nav-link-inner--text">Blog</span>
		         	</a>
		       	</li>
		       	@auth
			       	<li class="nav-item">
			         	<a href="{{ route('dashboard') }}" class="nav-link">
			           		<span class="nav-link-inner--text">Dashboard</span>
			         	</a>
			       	</li>
			       	<li class="nav-item">
			       		<form action="{{ route('logout') }}" method="post" id="logout">
			       			@csrf
			       			@method('POST')
			       			<a href="#" class="nav-link" onclick="$('#logout').submit()">
				           		<span class="nav-link-inner--text">Logout</span>
				         	</a>
			       		</form>
			       	</li>
		       	@else
			       	<li class="nav-item">
			         	<a href="{{ route('login') }}" class="nav-link">
			           		<span class="nav-link-inner--text">Login</span>
			         	</a>
			       	</li>
		       	@endauth
		    </ul>
     		<hr class="d-lg-none" />
			<ul class="navbar-nav align-items-lg-center ml-lg-auto">
				<li class="nav-item">
					<a class="nav-link nav-link-icon" href="{{$selve->facebook}}" target="_blank" data-toggle="tooltip" title="" data-original-title="Facebook">
						<i class="fab fa-facebook-square"></i>
						<span class="nav-link-inner--text d-lg-none">Facebook</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link nav-link-icon" href="{{$selve->ig}}" target="_blank" data-toggle="tooltip" title="" data-original-title="Instagram">
						<i class="fab fa-instagram"></i>
						<span class="nav-link-inner--text d-lg-none">Instagram</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link nav-link-icon" href="{{$selve->youtube}}" target="_blank" data-toggle="tooltip" title="" data-original-title="YouTube">
						<i class="fab fa-youtube"></i>
						<span class="nav-link-inner--text d-lg-none">YouTube</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link nav-link-icon" href="https://api.whatsapp.com/send/?phone={{$selve->phone}}" target="_blank" data-toggle="tooltip" title="" data-original-title="WhatsApp">
						<i class="fab fa-whatsapp"></i>
						<span class="nav-link-inner--text d-lg-none">WhatsApp</span>
					</a>
				</li>
			</ul>
   		</div>
 	</div>
</nav>