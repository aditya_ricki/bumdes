<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Theme tittle -->
    
    <meta name="description" content="@yield('meta_description', $meta_description)">
    <link rel="canonical" href="{{url()->current()}}"/>
    <title>@yield('title', $title) | Semar Mara</title>
    
    <!-- Theme style CSS -->     
    <link href="{{asset('front/css/bootstrap.min.css')}}" rel="stylesheet"> 
    <link href="{{asset('front/css/style.css')}}" rel="stylesheet"> 
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('front/images/favico/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('front/images/favico/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('front/images/favico/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('front/images/favico/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('front/images/favico/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('front/images/favico/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('front/images/favico/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('front/images/favico/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('front/images/favico/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('front/images/favico/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('front/images/favico/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('front/images/favico/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('front/images/favico/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('front/images/favico/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{asset('front/images/favico/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">
    <style>

        @media only screen and (max-width: 768px) {
            .wrapper-product {
                grid-template-columns: repeat(2, 1fr); 
                display: grid; 
                grid-auto-rows: minmax(100px, auto);
            }

            .wrapper-product .t_heding {
                font-size: 13px;
            }

            .wrapper-product .col {
                padding: 3px;
            }
        }

        @media only screen and (min-width: 768px) {
            .wrapper-product {
                grid-template-columns: repeat(4, 1fr); 
                display: grid; 
                grid-auto-rows: minmax(100px, auto);
            }
        }
        
    </style>
</head>
<body>
    <!-- Header Area -->
    @include('blog.partials.header')
    <!-- Header Area -->    

    @yield('content')
    
    <!-- Connect with us -->
    <section class="connect_with_us">
        <div class="container">
            <h2>Terhubung Dengan Kami</h2>
            <ul class="contact_with_socail">
                @php
                    $selve = App\Models\Selves::first();
                @endphp
                <li><a href="mailto:{{$selve->email}}"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="{{$selve->ig}}"><i class="fa fa-instagram"></i></a></li>
                <li><a href="https://api.whatsapp.com/send/?phone={{$selve->phone}}"><i class="fa fa-whatsapp"></i></a></li>
                <li><a href="{{$selve->facebook}}"><i class="fa fa-facebook"></i></a></li>
                <li><a href="{{$selve->youtube}}"><i class="fa fa-youtube"></i></a></li>
            </ul>
        </div>
    </section>
    <!-- End Connect with us -->
        
    @include('blog.partials.footer')
    
    <!-- Scroll Top Button -->
    <button class="scroll-top">
        <i class="fa fa-angle-double-up"></i>
    </button>
    
    <!-- Preloader -->
    <div class="preloader"></div>
    
    <!-- jQuery v3.2.1 -->
    <script src="{{asset('front/js/jquery-3.3.1.min.js')}}"></script> 
    <script src="{{asset('front/js/popper.min.js')}}"></script>
    <!-- Bootstrap v4.1.0 -->
    <script src="{{asset('front/js/bootstrap.min.js')}}"></script> 
    <!-- Extra Plugin -->
    <script src="{{asset('front/vendors/animate-css/wow.min.js')}}"></script> 
    
    <!-- Theme js / Custom js -->
    <script src="{{asset('front/js/theme.js')}}"></script>
</body>

</html>