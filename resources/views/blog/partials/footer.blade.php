<!-- Footer Area -->  
<footer class="footer_area">
    <div class="container">
        <div class="footer_inner row">
            @php
                $selve = APp\Models\Selves::first();
            @endphp
            <div class="col-lg-5 col-md-6 footer_logo">
                <a href="{{route('blog.home')}}"><img src="{{asset('front/images/logo.png')}}" alt="Semar Mara Blog"></a> 
                <p>{{$selve->description}}</p>
                <address>
                    <span>LOKASI</span>
                    <p>{{$selve->alamat}}</p>
                </address>
            </div>
        </div>
    </div>
    <div class="copy_right">
        <p>&copy; 2021 semarmara.com. All Right Reserved </p> 
    </div>
</footer>
<!-- End Footer Area -->  