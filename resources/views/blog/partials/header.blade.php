<header class="main_header_area" id="header"> 
    <div class="container">
        <div class="header_menu"> 
            <nav class="navbar navbar-expand-lg">
                <a class="navbar-brand" href="{{route('blog.home')}}"><img src="{{asset('front/images/logo.png')}}" alt=""></a>
                <!-- Small Divice Menu-->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar_supported"  aria-expanded="false" aria-label="Toggle navigation"> 
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </button>
                
                <div class="collapse navbar-collapse navbar_supported">
                    <ul class="navbar-nav"> 
                        <li>
                            <a class="nav-link" href="{{route('blog.home')}}" role="button">Home</a>
                        </li>  
                        <li><a class="nav-link" href="{{route('blog.tentang-kami')}}">Tentang Kami</a></li>  
                        <li class="dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">Artikel</a>
                            <ul class="dropdown-menu">
                                @php
                                   $articleTypes = App\Models\ArticleType::get()
                                @endphp
                                    
                                    <li><a href="{{route('articles')}}">Semua</a></li>
                                @foreach ($articleTypes as $item)
                                    <li><a href="{{route('article.byType', $item->slug)}}">{{$item->name}}</a></li>
                                @endforeach
                            </ul>
                        </li> 
                        <li class="dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">Produk</a>
                            <ul class="dropdown-menu">
                                @php
                                    $itemTypes = App\Models\ItemType::get()
                                @endphp
                                    <li><a href="{{route('products')}}">Semua</a></li>
                                @foreach ($itemTypes as $item)
                                    <li><a href="{{route('product.byType', $item->slug)}}">{{$item->name}}</a></li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="search_box">
                            <form action="{{route('blog.home')}}" method="GET">
                                <div class="input-group">
                                    <input type="text" name="text_search" class="form-control" placeholder="Cari">
                                    <div class="input-group-prepend">
                                        <button class="input-group-text"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </li>
                    </ul>  
                </div>
            </nav>  
            <ul class="search_button_content nav">
                <li class="dropdown search_dropbown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-search"></i></a>
                    <ul class="dropdown-menu">
                        <li>
                            <input type="text" placeholder="Search..">
                            <span><i class="fa fa-search"></i></span> 
                        </li>
                    </ul>
                </li>  
            </ul>  
        </div>
    </div>
</header>