<div class="topics_row row">
    <div class="col-12"><h2>Topik</h2></div>
    @php
        $topics = App\Models\ArticleType::get()
    @endphp
    
    @foreach ($topics as $topic)
        <div class="col-lg-4 col-md-6">
            <a href="#">{{$topic->name}}</a>
        </div>
    @endforeach
     
</div>