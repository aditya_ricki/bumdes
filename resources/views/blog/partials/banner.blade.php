<section class="home_banner_area">
    <div class="container">
        <div class="row home_banner_inner">
            <!-- Slicer col -->
            <div class="carousel slide banner_slider col-12" data-ride="carousel">
                <div class="carousel-inner">
                    @php
                        $banners = App\Services\BannerService::getBanners('home');
                    @endphp

                    @foreach ($banners as $banner)
                        <div class="carousel-item {{$banners[0] == $banner ? 'active' : ''}}">
                            <img src="{{asset($banner->path)}}">
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>