@extends('blog.app')

@section('content')
    <!-- Pages Heder Area --> 
    <div class="container" style="background-color: white;">
        <div class="pages_heder">
            <h2>Produk</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('blog.home')}}">Home</a></li> 
                <li><a href="{{route('products')}}" class="{{!$typeActive ? 'active' : ''}}">Produk</a></li>
                <li><a class="{{$typeActive ? 'active' : ''}}">{{$typeActive ?: ''}}</a></li>
                <li><a>{{$item->name}}</a></li>
            </ol>
        </div>
    </div> 
    <!-- End Pages Heder Area -->   
    
    <!-- Shop with sidebar -->
    <section class="shop_with_sidebar" style="background-color: white;">
        <div class="container"> 
            <div class="row">
                <div class="col-lg-3 shop_right_sidebar"> 
                    <form action="{{route('product.byType', $typeActive)}}" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control" name="text_search" value="{{old('text_search')}}" placeholder="Cari">
                            <div class="input-group-append">
                                <button class="input-group-text" type="submit"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                    <div class="s_widget">
                       <h4>Kategori :</h4>
                        <ul class="categories">
                            <li><a href="{{route('products')}}" style="color: {{!$typeActive ? 'red' : ''}}">Semua</a></li>
                            @foreach ($itemTypes as $itemType)
                                <li><a href="{{route('product.byType', $itemType->slug)}}" style="color: {{$typeActive == $itemType->slug ? 'red' : ''}}">{{$itemType->name}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9 shop_left_sidebar"> 
                    <div class="row">
                        <div class="col-md-6 min_img">
                            <img src="{{asset($item->path)}}" alt="{{$item->short_description}}">
                        </div>
                        <div class="col-md-6 product_details">
                            <h2>{{$item->name}}</h2>
                            <h1><b>@currency($item->selling_price)</b> <small>{{$item->stock > 0 ? 'Tersedia' : 'Habis'}}</small></h1>
                            <h4>Kategori: {{$item->item_type->name}} </h4>
                            <p>{{$item->short_description}}</p>
                        </div>
                        <div class="review-tab col-12">
                            <ul class="nav nav-tabs">
                                <li><a data-toggle="tab" href="#home" class="active theme_btn">Deskripsi</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="home" class="tab-pane fade in active show">
                                    {!! $item->description !!}
                                </div>
                            </div>
                        </div> 
                        <!-- /.review-tab --> 
                    </div>  
                </div>
            </div>
        </div>
    </section>
    <!-- End Shop with side bar -->

@endsection