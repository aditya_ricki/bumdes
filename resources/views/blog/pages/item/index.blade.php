@extends('blog.app')

@section('content')
    <!-- Pages Heder Area --> 
    <div class="container" style="background-color: white;">
        <div class="pages_heder">
            <h2>Produk</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('blog.home')}}">Home</a></li> 
                <li><a href="{{route('products')}}" class="{{!$typeActive ? 'active' : ''}}">Produk</a></li>
                <li><a class="{{$typeActive ? 'active' : ''}}">{{$typeActive ?: ''}}</a></li>
            </ol>
        </div>
    </div> 
    <!-- End Pages Heder Area -->   
    
    <!-- Shop banner Area -->   
    <section class="shop_banner" style="background-color: white;">
        <div class="container">
            <div class="row"> 
                <!-- Slicer col -->
                <div class="carousel slide banner_slider col-12" data-ride="carousel">
                    <div class="carousel-inner">
                        @php
                            $banners = App\Services\BannerService::getBanners('item');
                        @endphp
    
                        @foreach ($banners as $banner)
                            <div class="carousel-item {{$banners[0] == $banner ? 'active' : ''}}">
                                <img src="{{asset($banner->path)}}">
                            </div>
                        @endforeach
                    </div>
                </div>
                <!-- Slider -bottom -->
            </div>
        </div>
    </section>
    <!-- End shop banner Area -->   
    
    <!-- Shop with sidebar -->
    <section class="shop_with_sidebar" style="background-color: white;">
        <div class="container"> 
            <div class="row">
                <div class="col-lg-3 shop_right_sidebar">
                    <form action="" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control" name="text_search" placeholder="Cari">
                            <div class="input-group-append">
                                <button type="submit" class="input-group-text theme_btn"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                    <div class="s_widget">
                       <h4>Kategori :</h4>
                        <ul class="categories">
                            <li><a href="{{route('products')}}" style="color: {{!$typeActive ? 'red' : ''}}">Semua</a></li>
                            @foreach ($itemTypes as $item)
                                <li><a href="{{route('product.byType', $item->slug)}}" style="color: {{$typeActive == $item->slug ? 'red' : ''}}">{{$item->name}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9 shop_left_sidebar">
                    <div class="row wrapper-product">
                        @if (count($items) <= 0)
                            <h2 style="color: #C3B8A7;">Produk tidak ditemukan :(</h2>
                        @endif
                        @foreach ($items as $item)
                            <!-- shop_items -->
                            <div class="col"> 
                                <div class="shop_items"> 
                                    <a href="{{route('product', [$item->item_type->slug, $item->slug])}}" class="shop_img">
                                        <img src="{{asset($item->path)}}" alt="{{$item->name}}">
                                    </a>
                                    <div class="shop_text"> 
                                        <a href="{{route('product', [$item->item_type->slug, $item->slug])}}" class="s_heding">{{$item->name}}</a>
                                        <h6><b>@currency($item->selling_price)</b></h6>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <!-- Pagination_area--> 
                    <nav class="col-12 p-0">
                            {{$items->links()}}
                    </nav> 
                </div>
            </div>
        </div>
    </section>
    <!-- End Shop with side bar -->

@endsection