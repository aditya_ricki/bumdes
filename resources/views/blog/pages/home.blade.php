@extends('blog.app')

@section('content')

<!-- Home Banner Area --> 
@include('blog.partials.banner')
<!-- End Home Banner Area -->  

 <!-- Post section -->
<section class="post_section">
    <div class="container">
        <div class="row post_section_inner">
            <!-- Left_sidebar -->
            <div class="col left_sidebar">
                <!-- Tranding Area Post -->
                <div class="row tranding_post_area">
                    <div class="col-12 tranding_tittle"> 
                        <h2>Artikel</h2>
                        <a href="{{route('articles')}}">Lihat lebih banyak <i class="fa fa-arrow-right"></i></a>
                    </div>

                        <!-- Tranding_post -->
                    @foreach ($articles as $article)
                        <div class="col-md-6">
                            <div class="tranding_post"> 
                                <a href="{{route('article', [$article->article_type->slug, $article->slug])}}" class="post_img">
                                    <img src="{{asset($article->path)}}" alt="">
                                    <span class="tag_btn">{{$article->article_type->name}}</span>
                                </a>  
                                <div class="post_content"> 
                                    <a href="{{route('article', [$article->article_type->slug, $article->slug])}}" class="t_heding">{{$article->title}}</a>
                                    <h6>{{$article->created_at}} <span>|</span><a href="#">{{$article->user->name}}</a></h6>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row post_section_inner">
            <!-- Left_sidebar -->
            <div class="col-lg">
                <!-- Produk -->
                <div class="row">
                    <div class="col-12 tranding_tittle"> 
                        <h2>Produk Unggulan</h2>
                        <a href="{{route('products')}}">Lihat lebih banyak <i class="fa fa-arrow-right"></i></a>
                    </div>
                    <!-- product -->
                    <div class="wrapper-product">
                        @foreach ($items as $item)
                            <div class="col">
                                <div class="tranding_post"> 
                                    <a href="{{route('product', [$item->item_type->slug, $item->slug])}}" class="post_img">
                                        <img width="700" src="{{asset($item->path)}}" alt="">
                                        <span class="tag_btn">{{$item->item_type->name}}</span>
                                    </a>  
                                    <div class="post_content"> 
                                        <a href="{{route('product', [$item->item_type->slug, $item->slug])}}" class="t_heding">{{$item->name}}</a>
                                        <h6>stok : {{$item->stock}} <span>|</span><b>@currency($item->selling_price)</b></h6>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <!-- End left_sidebar -->
        </div>    
        <div class="row post_section_inner">
            <!-- Left_sidebar -->
            <div class="col-lg">
                <!-- Karyawan -->
                <div class="row">
                    <div class="col-12 tranding_tittle"> 
                        <h2>Anggota Kami</h2>
                    </div>
                    <!-- Tranding_post -->
                    <div class="wrapper-product">
                        @foreach ($users as $user)
                            <div class="col">
                                <div class="tranding_post"> 
                                    <div class="post_img">
                                        <img src="{{asset($user->path)}}" class="mx-auto d-block" alt="">
                                    </div>  
                                    <div class="post_content" style="min-height: 164px;">
                                        <div class="t_heding">{{$user->name}}</div>
                                        <h6>NIK <span>|</span>{{$user->nik}}</h6>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <!-- End left_sidebar -->
        </div>
    </div>
</section>
 <!-- End Post section -->
 @endsection