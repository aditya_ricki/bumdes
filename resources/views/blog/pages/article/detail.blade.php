@extends('blog.app')

@section('content')
    <!-- Pages Heder Area --> 
    <div class="container">
        <div class="pages_heder">
            <h2>Artikel</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('blog.home')}}">Home</a></li> 
                <li><a href="{{route('articles')}}">Artikel</a></li>
                <li><a href="{{route('article.byType', $typeActive)}}">{{$typeActive}}</a></li>
                <li><a >{{$article->title}}</a></li>
            </ol>
        </div>
    </div> 
    <!-- End Pages Heder Area --> 
    
    <!-- Post section -->
    <section class="post_section news_post">
        <div class="container">
            <div class="row post_section_inner">
                <!-- Left_sidebar --> 
                <div class="col-lg-8">  
                    <div class="news_left_sidebar">
                        <!-- News Item -->
                        <div class="news_item news_details">
                            <h6><span>{{$article->created_at}}</span> <a>Oleh : {{$article->user->name}}</a> <a class="investing">{{$article->article_type->name}}</a></h6>
                            <div class="news_heding">{{$article->title}}</div>
                            <img src="{{asset($article->path)}}" alt="">
                            {!! $article->long_description !!} 
                        </div> 
                        <div class="author_area">
                            <h2>Author</h2>
                            <div class="media">
                                <div class="media-left"> 
                                    <img src="{{asset($article->user->path)}}" alt="">
                                    <a href="#">{{$article->user->name}}</a>
                                </div>
                                <div class="media-body">
                                    <h6>{{$article->created_at}}</h6>
                                    <p>{!! $article->short_description !!}</p>
                                </div>
                            </div>
                        </div>
                        <div class="comment_area">
                            <h2>Komentar ({{count($article->comments)}})</h2>
                            @foreach ($article->comments as $item)
                                <div class="media">
                                    <div class="media-left"> 
                                        <a href="#">{{$item->name}}</a>
                                    </div>
                                    <div class="media-body">
                                        <p>{{$item->comment}}</p>
                                        <h6>{{$item->created_at}}</h6>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="leave_reply"> 
                           <h2>Tinggalkan Komentar</h2>
                            <form class="row login_from" action="{{route('comment.store')}}" method="POST">
                                @csrf
                                @method('POST')
                                <input type="hidden" name="article_id" value="{{$article->id}}"> 
                                <div class="form-group col-12">
                                    <textarea class="form-control" id="comment" name="comment" placeholder="Komentar Anda"></textarea>
                                </div>
                                <div class="form-group col-12">   
                                    <input type="text" class="form-control" name="name" placeholder="Nama"> 
                                </div> 
                                <div class="form-group col-12">
                                    <input type="email" class="form-control" name="email" placeholder="Email">
                                </div> 
                                <div class="form-group larg_btn col-12">
                                    <button class="sm_btn" type="submit">Submit <i class="fa fa-arrow-right"></i></button> 
                                </div>     
                            </form>  
                        </div>
                    </div>
                </div>
                
                <!-- End left_sidebar -->
                <div class="col-lg-4 right_sidebar">  
                    <form action="{{route('article.byType', $typeActive)}}" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control" name="text_search" value="{{old('text_search')}}" placeholder="Cari">
                            <div class="input-group-append">
                                <button class="input-group-text" type="submit"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                    <div class="categories">
                        <h3>Kategori</h3>
                        <ul class="cpost_categories">
                                <li><a href="{{route('articles')}}" class="{{(!$typeActive) ? 'active' : '' }}">Semua</a></li>
                            @foreach ($articleTypes as $item)
                                <li><a href="{{route('article.byType', $item->slug)}}" class="{{($typeActive && $typeActive == $item->slug) ? 'active' : '' }}">{{$item->name}}</a></li>
                            @endforeach
                        </ul>
                    </div> 
                </div>
            </div>
        </div>
    </section>
    <!-- End Post section -->
@endsection