@extends('blog.app')

@section('content')
    <!-- Pages Heder Area --> 
    <div class="container">
        <div class="pages_heder">
            <h2>Artikel</h2>
            <ol class="breadcrumb">
                <li><a href="{{route('blog.home')}}">Home</a></li> 
                <li><a href="{{route('articles')}}" class="{{!$typeActive ? 'active' : ''}}">Artikel</a></li>
                <li><a class="{{$typeActive ? 'active' : ''}}">{{$typeActive ?: ''}}</a></li>  
            </ol>
        </div>
    </div> 
    <!-- End Pages Heder Area --> 
    
    <!-- Post section -->
    <section class="post_section news_post">
        <div class="container">
            <div class="row post_section_inner">
                <!-- Left_sidebar --> 
                <div class="col-lg-8"> 
                    <div class="news_left_sidebar">
                        @if (count($articles) <= 0)
                            <h2 style="color: #C3B8A7;">Belum ada data :(</h2>
                        @endif
                        <!-- News Item -->
                        @foreach ($articles as $article)
                            <div class="news_item">
                                <h6>{{$article->created_at}}</h6>
                                <a href="{{route('article', [$article->article_type->slug, $article->slug])}}" class="news_heding">{{$article->title}}</a>
                                <img src="{{asset($article->path)}}">
                                <p>{!! $article->description_formated!!}</p>
                                <a href="{{route('article', [$article->article_type->slug, $article->slug])}}" class="red_btn">Baca Selengkapnya <i class="fa fa-arrow-right"></i></a>
                            </div>
                        @endforeach
                        <!-- News Item -->
                        <div class="col-12 p-0">
                                {{ $articles->links() }}
                        </div>
                    </div>
                </div>
                
                <!-- End left_sidebar -->
                <div class="col-lg-4 right_sidebar">  
                    <form action="" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control" name="text_search" value="{{old('text_search')}}" placeholder="Cari">
                            <div class="input-group-append">
                                <button class="input-group-text" type="submit"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                    <div class="categories">
                        <h3>Kategori</h3>
                        <ul class="cpost_categories">
                                <li><a href="{{route('articles')}}" class="{{(!$typeActive) ? 'active' : '' }}">Semua</a></li>
                            @foreach ($articleTypes as $item)
                                <li><a href="{{route('article.byType', $item->slug)}}" class="{{($typeActive && $typeActive == $item->slug) ? 'active' : '' }}">{{$item->name}}</a></li>
                            @endforeach
                        </ul>
                    </div> 
                </div>
            </div>
        </div>
    </section>
    <!-- End Post section -->
@endsection