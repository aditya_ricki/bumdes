@extends('blog.app')

@section('content')
<!-- Pages Heder Area --> 
<div class="container">
    <div class="pages_heder">
        <h2>Tentang Kami</h2>
        <ol class="breadcrumb">
            <li><a href="{{route('blog.home')}}">Home</a></li> 
            <li><a href="{{route('blog.tentang-kami')}}" class="active">Tentang Kami</a></li>
        </ol>
    </div>
</div> 
<!-- End Pages Heder Area -->  

<!-- About Content Area --> 
<section class="about_content" style="background-color: white;">
    <div class="container">
        {!! $selve->about_us !!}
        @include('blog.partials.topic')
    </div>
</section> 
<!-- End About Content Area --> 
@endsection
