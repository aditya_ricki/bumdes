@extends('layouts.dashboard.app')

@section('title', env('APP_NAME'))

@section('custom-css')
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection

@section('main-content')
<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Create article</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="{{ route('dashboard.article') }}">Users</a></li>
              <li class="breadcrumb-item active" aria-current="page">Create</li>
            </ol>
          </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
          <a href="{{ route('dashboard.article') }}" class="btn btn-sm btn-neutral"><i class="fas fa-arrow-left"></i> Back</a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid mt--6">
  <div class="row">
    <div class="col-xl-12 col-md-12 col-sm-12">
      <div class="card">
        <div class="card-body">
          <form action="{{ route('dashboard.article.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('POST')
            @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="form-group">
                  <label class="form-control-label" for="input-title">Title</label>
                  <input type="text" id="input-title" class="form-control" placeholder="Title" value="{{ old('title') }}" name="title">
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="form-group">
                  <label class="form-control-label" for="input-email">Type</label>
                  <select class="form-control" id="select2" name="article_type_id">
                    <option value="">Choose Article Type</option>
                    @foreach($articleTypes as $type)
                      <option value="{{ $type->id }}">{{ $type->name }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label class="form-control-label" for="input-email">Short Desc.</label>
                  <textarea class="form-control" name="short_description">{{ old('short_description') }}</textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label class="form-control-label" for="input-email">Long Desc.</label>
                  <textarea class="form-control summernote" name="long_description">{{ old('long_description') }}</textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="form-group">
                  <label class="form-control-label" for="input-email">Photo</label>
                  <input type="file" name="photo" id="photo" class="form-control">
                </div>
              </div>
            </div>
            <div class="row mt-3">
              <div class="col-md-12 col-sm-12">
                <button type="submit" class="btn btn-primary">Save</button>
                <a href="{{ route('dashboard.article') }}" class="btn btn-secondary">Cancel</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  @include('layouts.dashboard.footer')
</div>
@endsection

@section('custom-js')
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
  <script>
    $(document).ready(function() {
      $('#select2').select2()
      $('.summernote').summernote()
    })
  </script>
@endsection
