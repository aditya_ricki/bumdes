@extends('layouts.dashboard.app')

@section('title', env('APP_NAME'))

@section('main-content')
<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Income Statement</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
              <li class="breadcrumb-item active" aria-current="page"><a href="{{ route('dashboard.report') }}">Report</a></li>
              <li class="breadcrumb-item active" aria-current="page">Income Statement</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid mt--6">
  <div class="row">
    <div class="col-xl-12 col-md-12 col-sm-12">
      <div class="card">
      	<div class="card-header">
      		Report
      	</div>
        <div class="card-body">
        	<div class="row">
        		<div class="col-md-12 col-sm-12">
		          <div class="table-responsive">
		            <table class="table text-center" border="1" id="report">
		              <thead>
                    <tr>
  		                <th rowspan="2">Date</th>
  		                <th rowspan="2">Buyer</th>
                      <th rowspan="2">Selling Price</th>
                      <th rowspan="2">Capital Price</th>
                      <th colspan="5">Items</th>
                    </tr>
                    <tr>
                      <th>Name</th>
                      <th>QTY</th>
                      <th>Selling Price</th>
                      <th>Capital Price</th>
                    </tr>
		              </thead>
		              <tbody>
                    @foreach($reports as $report)
		                <tr>
                      <td>{{ $report['created_at'] }}</td>
		                  <td>{{ $report['buyer'] }}</td>
                      <td>@currency($report['total_selling_price'])</td>
                      <td>@currency($report['total_capital_price'])</td>
                      <td colspan="5">
                        <table class="table text-center" border="1">
                            @foreach($report['transaction_details'] as $detail)
                            <tr>
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $detail['item'] }}</td>
                              <td>{{ $detail['quantity'] }}</td>
                              <td>@currency($detail['selling_price'])</td>
                              <td>@currency($detail['capital_price'])</td>
                            </tr>
                            @endforeach
                        </table>
                      </td>
		                </tr>
                    @endforeach
                    <tr>
                      <th colspan="2">Total</th>
                      <td>@currency($total_capital_price)</td>
                      <td>@currency($total_selling_price)</td>
                      <td colspan="5">
                      </td>
                    </tr>
                    <tr>
                      <th colspan="2">Pendapatan</th>
                      <td colspan="2"><b>@currency($total_selling_price - $total_capital_price)</b></td>
                      <td colspan="5">
                      </td>
                    </tr>
		              </tbody>
		            </table>
		          </div>
        		</div>
        	</div>
        </div>
        <div class="card-footer">
        	<button class="btn btn-primary" type="button" onclick="exportTableToExcel()"><i class="fas fa-print"></i></button>
        </div>
      </div>
    </div>
  </div>

  @include('layouts.dashboard.footer')
</div>
@endsection

@section('custom-js')
<script>
  function exportTableToExcel(filename = new Date().getTime()){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById('report');
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

    // Specify file name
    filename = filename + '.xls';

    // Create download link element
    downloadLink = document.createElement("a");

    document.body.appendChild(downloadLink);

    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
        // Setting the file name
        downloadLink.download = filename;

        //triggering the function
        downloadLink.click();
    }
  }
</script>
@endsection
