@extends('layouts.dashboard.app')

@section('title', env('APP_NAME'))

@section('main-content')
<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Edit user</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="{{ route('dashboard.user') }}">Users</a></li>
              <li class="breadcrumb-item active" aria-current="page">Edit</li>
            </ol>
          </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
          <a href="{{ route('dashboard.user') }}" class="btn btn-sm btn-neutral"><i class="fas fa-arrow-left"></i> Back</a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid mt--6">
  <div class="row">
    <div class="col-xl-12 col-md-12 col-sm-12">
      <div class="card">
        <div class="card-body">
          <form action="{{ route('dashboard.user.update', $result->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="form-group">
                  <label class="form-control-label" for="input-name">Name</label>
                  <input type="text" id="input-name" class="form-control" placeholder="Name" value="{{ $result->name }}" name="name">
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="form-group">
                  <label class="form-control-label" for="input-nik">NIK</label>
                  <input type="text" id="input-nik" class="form-control" placeholder="NIK" value="{{ $result->nik }}" name="nik">
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="form-group">
                  <label class="form-control-label" for="input-email">Email</label>
                  <input type="email" id="input-email" class="form-control" placeholder="Email" value="{{ $result->email }}" name="email">
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="form-group">
                  <label class="form-control-label" for="input-phone">Phone</label>
                  <input type="text" id="input-phone" class="form-control" placeholder="Phone" value="{{ $result->phone }}" name="phone">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="form-group">
                  <label class="form-control-label" for="input-password">Password</label>
                  <input type="password" id="input-password" class="form-control" placeholder="Password" name="password">
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="form-group">
                  <label class="form-control-label" for="input-password-confirmation">Password Confirmation</label>
                  <input type="password" id="input-password-confirmation" class="form-control" placeholder="Password Confirmation" name="password_confirmation">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col">
                <img src="{{asset($result->path)}}" width="200" alt="">
              </div>
            </div>
            <div class="row">
              <div class="col">
                <label class="form-control-label" for="input-photo">Foto</label>
                <input type="file" id="input-photo" class="form-control" placeholder="photo" value="{{ old('photo') }}" name="photo">
              </div>
            </div>
            <div class="row mt-3">
              <div class="col-md-12 col-sm-12">
                <button type="submit" class="btn btn-primary">Save</button>
                <a href="{{ route('dashboard.user') }}" class="btn btn-secondary">Cancel</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  @include('layouts.dashboard.footer')
</div>
@endsection
