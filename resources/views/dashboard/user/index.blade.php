@extends('layouts.dashboard.app')

@section('title', env('APP_NAME'))

@php
	$perPage = request()->per_page ?: 10;
@endphp

@section('main-content')
<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Users</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
              <li class="breadcrumb-item active" aria-current="page">Users</li>
            </ol>
          </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
          <a href="{{ route('dashboard.user.create') }}" class="btn btn-sm btn-neutral"><i class="fas fa-plus"></i> Create</a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid mt--6">
  <div class="row">
    <div class="col-xl-12 col-md-12 col-sm-12">
      <div class="card">
      	<div class="card-header">
      		<form action="{{ route('dashboard.user') }}" method="post">
      			@csrf
      			@method('GET')
	      		<div class="row">
	        		<div class="col-md-6 col-sm-12">
	        			<div class="form-group text-sm">
	        				Show
	        				<select name="per_page" class="form-control form-control-sm d-inline" style="width: 70px;">
	        					<option value="10" {{ $perPage != 10 ?: 'selected' }}>10</option>
	        					<option value="25" {{ $perPage != 25 ?: 'selected' }}>25</option>
	        					<option value="50" {{ $perPage != 50 ?: 'selected' }}>50</option>
	        					<option value="100" {{ $perPage != 100 ?: 'selected' }}>100</option>
	        				</select>
	        				data
	        			</div>
	        		</div>
	        		<div class="col-md-6 col-sm-12">
	        			<div class="float-right">
	        				<div class="input-group text-sm float-right">
	        					<input type="text" class="form-control form-control-sm" placeholder="Keyword..." name="text_search">
	        					<div class="input-group-append">
									    <button class="btn btn-primary btn-sm" type="submit" id="button-addon2">Filter</button>
									  </div>
	        				</div>
	        			</div>
	        		</div>
	        	</div>
	        </form>
      	</div>
        <div class="card-body">
        	<div class="row">
        		<div class="col-md-12 col-sm-12">
		          <div class="table-responsive">
		            <table class="table table-striped table-hover table-borderless">
		              <thead>
		                <th>No</th>
		                <th>Name</th>
		                <th>Phone</th>
		                <th>Email</th>
		                <th></th>
		              </thead>
		              <tbody>
		                @foreach($results as $result)
		                <tr>
		                  <td>{{ $loop->iteration }}</td>
		                  <td>{{ $result->name }}</td>
		                  <td>{{ $result->phone }}</td>
		                  <td>{{ $result->email }}</td>
		                  <td>
		                    <form action="{{ route('dashboard.user.destroy', $result->id) }}" method="post" id="form-{{ $result->id }}">
		                      @csrf
		                      @method('DELETE')
		                      <a href="{{ route('dashboard.user.edit', $result->id) }}" class="btn btn-success btn-sm text-white">
		                        <i class="fas fa-edit"></i>
		                      </a>
		                      <button type="button" class="btn btn-danger btn-sm text-white btn-hapus" data-id="{{ $result->id }}">
		                        <i class="fas fa-trash"></i>
		                      </button>
		                    </form>
		                  </td>
		                </tr>
		                @endforeach
		              </tbody>
		            </table>
		          </div>
        		</div>
        	</div>
        </div>
        <div class="card-footer">
        	<div class="row">
        		<div class="col-md-6 col-sm-12 text-sm">
        			Showing {{ $results->firstItem() }} to {{ $results->lastItem() }} of {{ $results->total() }} result
        		</div>
        		<div class="col-md-6 col-sm-12">
        			<div class="float-right">
        				{{ $results->links() }}
        			</div>
        		</div>
        	</div>
        </div>
      </div>
    </div>
  </div>

  @include('layouts.dashboard.footer')
</div>
@endsection

@section('custom-js')
<script>
  $('.btn-hapus').on('click', function (e) {
    let id = e.target.dataset['id']

    Swal.fire({
      title: 'Warning',
      text: 'Are you sure?',
      icon: 'warning',
      showCancelButton: true,
    }).then((data) => {
      if (data.isConfirmed) {
        $(`#form-${id}`).submit()
      }
    })
  })
</script>
@endsection
