@extends('layouts.dashboard.app')

@section('title', env('APP_NAME'))

@php
	$perPage = request()->per_page ?: 10;
@endphp

@section('main-content')
<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Arsip Surat</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
              <li class="breadcrumb-item active" aria-current="page">Arsip Surat</li>
            </ol>
          </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
          <a href="#" class="btn btn-sm btn-neutral" data-toggle="modal" data-target="#modal-create"><i class="fas fa-plus"></i> Create</a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid mt--6">
  <div class="row">
    <div class="col-xl-12 col-md-12 col-sm-12">
      <div class="card">
      	<div class="card-header">
      		<form action="{{ route('dashboard.letter') }}" method="post">
      			@csrf
      			@method('GET')
	      		<div class="row">
	        		<div class="col-md-6 col-sm-12">
	        			<div class="form-group text-sm">
	        				Show
	        				<select name="per_page" class="form-control form-control-sm d-inline" style="width: 70px;">
	        					<option value="10" {{ $perPage != 10 ?: 'selected' }}>10</option>
	        					<option value="25" {{ $perPage != 25 ?: 'selected' }}>25</option>
	        					<option value="50" {{ $perPage != 50 ?: 'selected' }}>50</option>
	        					<option value="100" {{ $perPage != 100 ?: 'selected' }}>100</option>
	        				</select>
	        				data
	        			</div>
	        		</div>
	        		<div class="col-md-6 col-sm-12">
	        			<div class="float-right">
	        				<div class="input-group text-sm float-right">
	        					<input type="text" class="form-control form-control-sm" placeholder="Keyword..." name="text_search">
	        					<div class="input-group-append">
									    <button class="btn btn-primary btn-sm" type="submit" id="button-addon2">Filter</button>
									  </div>
	        				</div>
	        			</div>
	        		</div>
	        	</div>
	        </form>
      	</div>
        <div class="card-body">
        	<div class="row">
        		<div class="col-md-12 col-sm-12">
		          <div class="table-responsive">
		            <table class="table table-striped table-hover table-borderless">
		              <thead>
		                <th>No</th>
		                <th>Pengirim</th>
                    <th>Perihal</th>
                    <th>Tgl Surat</th>
                    <th>File</th>
		                <th></th>
		              </thead>
		              <tbody>
                  {{-- MODAL CREATE --}}
                  <div class="modal fade" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <form action="{{ route('dashboard.letter.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('POST')
                        <div class="modal-content">
                          <div class="modal-header">
                            <h6 class="modal-title" id="modal-title-default">Create data</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <div class="col-md-12 col-sm-12">
                                <div class="form-group">
                                  <label class="form-control-label" for="input-sender">Pengirim</label>
                                  <input type="text" class="form-control" id="input-sender" name="sender" value="{{ old('sender') }}" placeholder="Pengirim">
                                </div>
                              </div>
                              <div class="col-md-12 col-sm-12">
                                <div class="form-group">
                                  <label class="form-control-label" for="input-content">Perihal</label>
                                  <textarea name="content" class="form-control" id="input-content" cols="30" rows="10">{{ old('content') }}</textarea>
                                </div>
                              </div>
                              <div class="col-md-12 col-sm-12">
                                <div class="form-group">
                                  <label class="form-control-label" for="input-tgl">Tanggal Surat</label>
                                  <input type="text" class="form-control" id="input-tgl" name="date" value="{{ old('date') }}" placeholder="Tgl Surat">
                                </div>
                              </div>
                              <div class="col-md-12 col-sm-12">
                                <div class="form-group">
                                  <label class="form-control-label" for="input-file">File</label>
                                  <input type="file" class="form-control" id="input-file" name="file" value="{{ old('path') }}" placeholder="File">
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
		                @foreach($results as $result)
                    <div class="modal fade" id="modal-edit-{{ $result->id }}" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
                      {{-- MODAL EDIT --}}
                      <div class="modal-dialog modal-dialog-centered" role="document">
                        <form action="{{ route('dashboard.letter.update', $result->id) }}" method="POST" enctype="multipart/form-data">
                          @csrf
                          @method('PUT')
                          <div class="modal-content">
                            <div class="modal-header">
                              <h6 class="modal-title" id="modal-title-default">Update data</h6>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <div class="col-md-12 col-sm-12">
                                  <div class="form-group">
                                    <label class="form-control-label" for="input-sender-{{ $result->id }}">Pengirim</label>
                                    <input type="text" class="form-control" id="input-sender-{{ $result->id }}" name="sender" value="{{ $result->sender }}" placeholder="Pengirim">
                                  </div>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                  <div class="form-group">
                                    <label class="form-control-label" for="input-content-{{ $result->id }}">Content</label>
                                    <textarea name="content" class="form-control" id="input-content-{{ $result->id }}" cols="30" rows="10">{{ $result->content }}</textarea>
                                  </div>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                  <div class="form-group">
                                    <label class="form-control-label" for="input-date-{{ $result->id }}">Tgl Surat</label>
                                    <input type="text" class="form-control" id="input-date-{{ $result->id }}" name="date" value="{{ $result->date }}" placeholder="Tgl Surat">
                                  </div>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                  <div class="form-group">
                                    <label class="form-control-label" for="input-file">File</label>
                                    <input type="file" class="form-control" id="input-file" name="file" value="{{ old('path') }}" placeholder="File">
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="submit" class="btn btn-primary">Save</button>
                              <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
		                <tr>
		                  <td>{{ $loop->iteration }}</td>
		                  <td>{{ $result->sender }}</td>
                      <td>{{ $result->short_content_formated }}</td>
                      <td>{{ $result->date }}</td>
                      <td>
                        <a class="btn btn-primary btn-sm text-white" href="{{ asset($result->path) }}" target="_blank"><i class="fas fa-eye"></i> Lihat Berkas</a>
                      </td>
		                  <td>
		                    <form action="{{ route('dashboard.letter.destroy', $result->id) }}" method="post" enctype="multipart/form-data" id="form-{{ $result->id }}">
		                      @csrf
		                      @method('DELETE')
		                      <a href="#" class="btn btn-success btn-sm text-white" data-toggle="modal" data-target="#modal-edit-{{ $result->id }}">
		                        <i class="fas fa-edit"></i>
		                      </a>
		                      <button type="button" class="btn btn-danger btn-sm text-white btn-hapus" data-id="{{ $result->id }}">
		                        <i class="fas fa-trash"></i>
		                      </button>
		                    </form>
		                  </td>
		                </tr>
		                @endforeach
		              </tbody>
		            </table>
		          </div>
        		</div>
        	</div>
        </div>
        <div class="card-footer">
        	<div class="row">
        		<div class="col-md-6 col-sm-12 text-sm">
        			Showing {{ $results->firstItem() }} to {{ $results->lastItem() }} of {{ $results->total() }} result
        		</div>
        		<div class="col-md-6 col-sm-12">
        			<div class="float-right">
        				{{ $results->links() }}
        			</div>
        		</div>
        	</div>
        </div>
      </div>
    </div>
  </div>

  @include('layouts.dashboard.footer')
</div>
@endsection

@section('custom-js')
<script>
  $('.btn-hapus').on('click', function (e) {
    let id = e.target.dataset['id']

    Swal.fire({
      title: 'Warning',
      text: 'Are you sure?',
      icon: 'warning',
      showCancelButton: true,
    }).then((data) => {
      if (data.isConfirmed) {
        $(`#form-${id}`).submit()
      }
    })
  })
</script>
@endsection
