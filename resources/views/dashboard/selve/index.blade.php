@extends('layouts.dashboard.app')

@section('title', env('APP_NAME'))

@section('custom-css')
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection

@section('main-content')
<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Semarmara Blog</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="{{ route('dashboard.selve') }}">Semarmara Blog</a></li>
              <li class="breadcrumb-item active" aria-current="page">Update</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid mt--6">
  <div class="row">
    <div class="col-xl-12 col-md-12 col-sm-12">
      <div class="card">
        <div class="card-body">
          <form action="{{ route('dashboard.selve.update', $result->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif
            <input type="hidden" value="{{$result->id}}" id="id">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                  <div class="form-group">
                    <label class="form-control-label" for="input-title-{{ $result->id }}">Title</label>
                    <input type="text" class="form-control" id="input-title-{{ $result->id }}" name="title" value="{{ $result->title }}" placeholder="Title">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-sm-12">
                  <div class="form-group">
                    <label class="form-control-label" for="input-desription">Description</label>
                    <textarea class="form-control" id="input-description-{{ $result->id }}" name="description">{{ $result->description }}</textarea>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-sm-12">
                  <div class="form-group">
                    <label class="form-control-label" for="input-about_us">About Us</label>
                    <textarea class="form-control summernote" id="input-about-{{ $result->id }}" name="about_us">{{ $result->about_us }}</textarea>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-sm-12">
                  <div class="form-group">
                    <label class="form-control-label" for="input-motto-{{ $result->id }}">Motto</label>
                    <textarea type="text" class="form-control" id="input-motto-{{ $result->id }}" name="motto" placeholder="Motto">{{ $result->motto }}</textarea>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-sm-12">
                  <div class="form-group">
                    <label>Banners Home</label>
                    <div class="card" style="min-height: 210px;">
                      <div class="row wrapper-product files-home" id="files">
                        @foreach ($result->banners as $banner)
                          @if ($banner->role == 'home')
                            <div class="col" id="banner-{{$banner->id}}">
                              <img src="{{asset($banner->path)}}" width="200" style="cursor: pointer;" data-id="{{ $banner->id }}" class="img-fluid hps-banner">
                            </div>
                          @endif
                        @endforeach
                      </div>
                    </div>
                    <input type="file" accept="image/*" id="input-banner-home" multiple data-role="home" style="display: none;">
                    <label for="input-banner-home" class="btn btn-primary">Upload Banner Home</label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-sm-12">
                  <div class="form-group">
                    <label>Banners Item</label>
                    <div class="card" style="min-height: 210px;">
                      <div class="row wrapper-product files-item" id="files">
                        @foreach ($result->banners as $banner)
                          @if ($banner->role == 'item')
                            <div class="col" id="banner-{{$banner->id}}">
                              <img src="{{asset($banner->path)}}" width="200" style="cursor: pointer;" data-id="{{ $banner->id }}" class="img-fluid hps-banner">
                            </div>
                          @endif
                        @endforeach
                      </div>
                    </div>
                    <input type="file" accept="image/*" id="input-banner-item" multiple data-role="item" style="display: none;">
                    <label for="input-banner-item" class="btn btn-primary">Upload Banner Item</label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-sm-12">
                  <div class="form-group">
                    <label class="form-control-label" for="input-alamat-{{ $result->id }}">Alamat</label>
                    <textarea type="text" class="form-control" id="input-alamat-{{ $result->id }}" name="alamat" placeholder="alamat">{{ $result->alamat }}</textarea>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <div class="form-group">
                    <label class="form-control-label" for="input-email-{{ $result->id }}">Email</label>
                    <input type="email" class="form-control" id="input-email-{{ $result->id }}" name="email" value="{{ $result->email }}" placeholder="Email">
                  </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="form-group">
                      <label class="form-control-label" for="input-phone-{{ $result->id }}">Phone</label>
                      <input type="text" class="form-control" id="input-phone-{{ $result->id }}" name="phone" value="{{ $result->phone }}" placeholder="Phone">
                    </div>
                  </div>
              </div>
              <div class="row">
                <div class="col-md-4 col-sm-4">
                  <div class="form-group">
                    <label class="form-control-label" for="input-ig-{{ $result->id }}">Instagram</label>
                    <input type="text" class="form-control" id="input-ig-{{ $result->id }}" name="ig" value="{{ $result->ig }}" placeholder="instagram">
                  </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="form-group">
                      <label class="form-control-label" for="input-facebook-{{ $result->id }}">Facebook</label>
                      <input type="text" class="form-control" id="input-facebook-{{ $result->id }}" name="facebook" value="{{ $result->facebook }}" placeholder="Facebook">
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                  <div class="form-group">
                     <label class="form-control-label" for="input-youtube-{{ $result->id }}">Youtube</label>
                     <input type="text" class="form-control" id="input-youtube-{{ $result->id }}" name="youtube" value="{{ $result->youtube }}" placeholder="Youtube">
                    </div>
                </div>
              </div>
            <div class="row mt-3">
              <div class="col-md-12 col-sm-12">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  @include('layouts.dashboard.footer')
</div>
@endsection

@section('custom-js')
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
  <script>
    $(document).ready(function() {
      $('#select2').select2();

      $('.summernote').summernote();

      $(".btn-success").click(function(){ 
          var lsthmtl = $(".clone").html();
          $(".increment").after(lsthmtl);
      });

      $("body").on("click",".btn-danger",function(){ 
          $(this).parents(".hdtuto").remove();
      });

      function ajaxUploadPhoto(formdata) {
        return new Promise((resolve, reject)=>{
          $.ajax({
            url: "/dashboard/selve/banner",
            type: "POST",
            processData: false,
            contentType: false,
            data: formdata
          })
          .done(resolve)
          .fail(reject);
        });
      }

      function setUploadPhoto(event) {
        
        let role = event.target.dataset['role'];

        loadingImage(event.target.files[0], 'add', role);

        let formData = new FormData();
        let csrf = $('input[name$="_token"]').val();
        let id = $('#id').val();
        

        formData.append('photo', event.target.files[0]);
        formData.append('role', role);
        formData.append('selve_id', id);
        formData.append('_token', csrf);

        ajaxUploadPhoto(formData).then((res)=>{
          loadingImage('', 'remove', role);
          $(`.files-${res.role}`).append(`
            <div class="col" id="banner-${res.id}">
              <img src="/${res.path}" style="cursor: pointer;" width="200" data-id="${res.id}" class="img-fluid hps-banner">
            </div>
          `);
        }).catch((res)=>{
          loadingImage('', 'remove', role);
          Swal.fire({
            title: 'Warning',
            text: 'Gagal Upload Banner',
            icon: 'warning'
          })
        })
      }

      function loadingImage(data = '', con, role){
        switch (con) {
          case 'add':
              $(`.files-${role}`).append(`
                <div class="col" id="loading">
                  <img src="${URL.createObjectURL(data)}" style="cursor: pointer; opacity: 0.2"; width="200">
                </div>
              `);
            break;
          case 'remove':
              $('#loading').remove();
            break;
          default:
            break;
        }
      }

      //input gambar banner
      $("#input-banner-home").change((event)=>{
        setUploadPhoto(event);
      });
      $("#input-banner-item").change((event)=>{
        setUploadPhoto(event);
      });

      //hapus banner
      $('.wrapper-product').on('click', '.hps-banner', (e)=>{
        let id = e.target.dataset['id'];
        let csrf = $('input[name$="_token"]').val();
        $(`#banner-${id}`).css('opacity', 0.2);

        if(id){
          $.ajax({
              url: `/dashboard/selve/banner/${id}`,
              type: "DELETE",
              data: {
                _token: csrf
              },
              success: (res) => {
                $(`#banner-${id}`).remove();
              },
              error: (res) => {
                Swal.fire({
                  title: 'Warning',
                  text: 'Gagal Hapus Banner',
                  icon: 'warning'
                })
              }
          });
        }
      })

    })
  </script>
@endsection
