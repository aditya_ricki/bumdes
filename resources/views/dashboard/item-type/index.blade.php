@extends('layouts.dashboard.app')

@section('title', env('APP_NAME'))

@php
	$perPage = request()->per_page ?: 10;
@endphp

@section('main-content')
<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Item Type</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
              <li class="breadcrumb-item active" aria-current="page">Item Type</li>
            </ol>
          </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
          <a href="#" class="btn btn-sm btn-neutral" data-toggle="modal" data-target="#modal-create"><i class="fas fa-plus"></i> Create</a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid mt--6">
  <div class="row">
    <div class="col-xl-12 col-md-12 col-sm-12">
      <div class="card">
      	<div class="card-header">
      		<form action="{{ route('dashboard.item-type') }}" method="post">
      			@csrf
      			@method('GET')
	      		<div class="row">
	        		<div class="col-md-6 col-sm-12">
	        			<div class="form-group text-sm">
	        				Show
	        				<select name="per_page" class="form-control form-control-sm d-inline" style="width: 70px;">
	        					<option value="10" {{ $perPage != 10 ?: 'selected' }}>10</option>
	        					<option value="25" {{ $perPage != 25 ?: 'selected' }}>25</option>
	        					<option value="50" {{ $perPage != 50 ?: 'selected' }}>50</option>
	        					<option value="100" {{ $perPage != 100 ?: 'selected' }}>100</option>
	        				</select>
	        				data
	        			</div>
	        		</div>
	        		<div class="col-md-6 col-sm-12">
	        			<div class="float-right">
	        				<div class="input-group text-sm float-right">
	        					<input type="text" class="form-control form-control-sm" placeholder="Keyword..." name="text_search">
	        					<div class="input-group-append">
									    <button class="btn btn-primary btn-sm" type="submit" id="button-addon2">Filter</button>
									  </div>
	        				</div>
	        			</div>
	        		</div>
	        	</div>
	        </form>
      	</div>
        <div class="card-body">
        	<div class="row">
        		<div class="col-md-12 col-sm-12">
		          <div class="table-responsive">
		            <table class="table table-striped table-hover table-borderless">
		              <thead>
		                <th>No</th>
		                <th>Name</th>
		                <th></th>
		              </thead>
		              <tbody>
                  {{-- MODAL CREATE --}}
                  <div class="modal fade" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <form action="{{ route('dashboard.item-type.store') }}" method="POST">
                        @csrf
                        @method('POST')
                        <div class="modal-content">
                          <div class="modal-header">
                            <h6 class="modal-title" id="modal-title-default">Create data</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <div class="col-md-12 col-sm-12">
                                <div class="form-group">
                                  <label class="form-control-label" for="input-name">Name</label>
                                  <input type="text" class="form-control" id="input-name" name="name" value="{{ old('name') }}" placeholder="Name">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12 col-sm-12">
                                <div class="form-group">
                                  <label class="form-control-label" for="input-desription">Description</label>
                                  <textarea type="text" class="form-control" id="input-description" name="short_description" placeholder="Description">{{ old('short_description') }}</textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
		                @foreach($results as $result)
                    <div class="modal fade" id="modal-edit-{{ $result->id }}" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
                      {{-- MODAL EDIT --}}
                      <div class="modal-dialog modal-dialog-centered" role="document">
                        <form action="{{ route('dashboard.item-type.update', $result->id) }}" method="POST">
                          @csrf
                          @method('PUT')
                          <div class="modal-content">
                            <div class="modal-header">
                              <h6 class="modal-title" id="modal-title-default">Update data</h6>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <div class="col-md-12 col-sm-12">
                                  <div class="form-group">
                                    <label class="form-control-label" for="input-name-{{ $result->id }}">Name</label>
                                    <input type="text" class="form-control" id="input-name-{{ $result->id }}" name="name" value="{{ $result->name }}" placeholder="Name">
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-12 col-sm-12">
                                  <div class="form-group">
                                    <label class="form-control-label" for="input-desription">Description</label>
                                    <textarea type="text" class="form-control" id="input-description-{{ $result->id }}" name="short_description" placeholder="Description">{{ $result->short_description }}</textarea>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="submit" class="btn btn-primary">Save</button>
                              <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
		                <tr>
		                  <td>{{ $loop->iteration }}</td>
		                  <td>{{ $result->name }}</td>
		                  <td>
		                    <form action="{{ route('dashboard.item-type.destroy', $result->id) }}" method="post" id="form-{{ $result->id }}">
		                      @csrf
		                      @method('DELETE')
		                      <a href="#" class="btn btn-success btn-sm text-white" data-toggle="modal" data-target="#modal-edit-{{ $result->id }}">
		                        <i class="fas fa-edit"></i>
		                      </a>
		                      <button type="button" class="btn btn-danger btn-sm text-white btn-hapus" data-id="{{ $result->id }}">
		                        <i class="fas fa-trash"></i>
		                      </button>
		                    </form>
		                  </td>
		                </tr>
		                @endforeach
		              </tbody>
		            </table>
		          </div>
        		</div>
        	</div>
        </div>
        <div class="card-footer">
        	<div class="row">
        		<div class="col-md-6 col-sm-12 text-sm">
        			Showing {{ $results->firstItem() }} to {{ $results->lastItem() }} of {{ $results->total() }} result
        		</div>
        		<div class="col-md-6 col-sm-12">
        			<div class="float-right">
        				{{ $results->links() }}
        			</div>
        		</div>
        	</div>
        </div>
      </div>
    </div>
  </div>

  @include('layouts.dashboard.footer')
</div>
@endsection

@section('custom-js')
<script>
  $('.btn-hapus').on('click', function (e) {
    let id = e.target.dataset['id']

    Swal.fire({
      title: 'Warning',
      text: 'Are you sure?',
      icon: 'warning',
      showCancelButton: true,
    }).then((data) => {
      if (data.isConfirmed) {
        $(`#form-${id}`).submit()
      }
    })
  })
</script>
@endsection
