@extends('layouts.dashboard.app')

@section('title', env('APP_NAME'))

@section('custom-css')
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css">
@endsection

@php
  $perPage = request()->per_page ?: 10;
@endphp

@section('main-content')
<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Sale</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
              <li class="breadcrumb-item active" aria-current="page">Sale</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid mt--6">
  <div class="row">
    <div class="col-xl-5 col-md-5 col-sm-12">
      <div class="card">
        <div class="card-body">
            <div class="table-responsive">
              <table id="tb-items" class="table table-borderless">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($items as $item)
                    <tr>
                      <td>{{ $loop->iteration }}</td>
                      <td>{{ $item->name }}</td>
                      <td>
                        <button data-id="{{ $item->id }}" data-name="{{ $item->name }}" data-capital-price="{{ $item->capital_price }}" data-selling-price="{{ $item->selling_price }}" class="btn btn-sm btn-success addItem" id="btnAddItem-{{ $item->id }}">
                          <i class="fas fa-plus"></i>
                        </button>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-7 col-md-7 col-sm-12">
        <form action="{{ route('dashboard.transaction.sale') }}" method="POST">
          @csrf
          @method('POST')
          <div class="card">
            <div class="card-header">
              Sale
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="form-control-label">Buyer name</label>
                    <input type="text" class="form-control" name="buyer">
                  </div>
                </div>
              </div>
              <div class="table-responsive">
                <table id="tb-items-select" class="table table-borderless">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Price</th>
                      <th>QTY</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody id="form-restock">
                  </tbody>
                </table>
              </div>
            </div>
            <div class="card-footer">
              <button class="btn btn-primary btn-sm">
                <i class="fas fa-save"></i>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
    @include('layouts.dashboard.footer')
  </div>
@endsection

@section('custom-js')
  {{-- <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script> --}}
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
  <script>
  $(document).ready( function () {
    $('#tb-items').DataTable({
      'bLengthChange': false,
      'bInfo': false,
      'language': {
        'paginate': {
          'previous': '<',
          'next': '>'
        }
      }
    })

    $('.addItem').on('click', function () {
      let newElement = `
      <tr id="trItem-${$(this).data('id')}">
        <td>
          ${$(this).data('name')}
          <div class="form-group">
            <input type="text" class="form-control d-none" value="${$(this).data('id')}" name="id[]">
          </div>
        </td>
        <td>
          ${$(this).data('selling-price')}
        </td>
        <td>
          <div class="form-group">
            <input type="number" class="form-control" name="qty[]" min="0" value="0">
          </div>
        </td>
        <td>
          <button class="btn btn-sm btn-danger" onclick="removeItem(${$(this).data('id')})" type="button">
            <i class="fas fa-times"></i>
          </button>
        </td>
      </tr>`

      $('#form-restock').append(newElement)

      $(`#btnAddItem-${$(this).data('id')}`).addClass('d-none')
    })
  })

  function removeItem (id) {
    $(`#btnAddItem-${id}`).removeClass('d-none')
    $(`#trItem-${id}`).remove()
  }
  </script>
@endsection
