<?php

use Illuminate\Support\Facades\Route;

/**
 * DASHBOARD
 */
use App\Http\Controllers\Dashboard\HomeController as DashboardHome;
use App\Http\Controllers\Dashboard\UserController as DashboardUser;
use App\Http\Controllers\Dashboard\ItemTypeController as DashboardItemType;
use App\Http\Controllers\Dashboard\ItemController as DashboardItem;
use App\Http\Controllers\Dashboard\ArticleTypeController as DashboardArticleType;
use App\Http\Controllers\Dashboard\ArticleController as DashboardArticle;
use App\Http\Controllers\Dashboard\SuplierController as DashboardSuplier;
use App\Http\Controllers\Dashboard\LetterController as DashboardLetter;
use App\Http\Controllers\Dashboard\TransactionController as DashboardTransaction;
use App\Http\Controllers\Dashboard\SelveController as DashboardSelve;
use App\Http\Controllers\Dashboard\BannerController as DashboardBanner;
use App\Http\Controllers\Dashboard\CostController as DashboardCost;
use App\Http\Controllers\Dashboard\ReportController as DashboardReport;
use App\Http\Controllers\Dashboard\ModalController as DashboardModal;

/**
 * BLOG
 */
use App\Http\Controllers\blog\HomeController as BlogHome;
use App\Http\Controllers\blog\ArticleController as BlogArticle;
use App\Http\Controllers\blog\CommentController as BlogComment;
use App\Http\Controllers\blog\ItemController as BlogItem;
use App\Http\Controllers\blog\SelveController as BlogSelve;
/**
 * LANDING PAGE
 */
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
	'register' => false,
	'verify' => true,
]);

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::group(['prefix' => 'dashboard', 'middleware' => ['auth']], function () {
	Route::get('/', [DashboardTransaction::class, 'selling'])->name('dashboard');

	/**
	 * MANAGE USER
	 */
	Route::group(['prefix' => 'user'], function () {
		Route::get('/', [DashboardUser::class, 'index'])->name('dashboard.user');
		Route::get('/create', [DashboardUser::class, 'create'])->name('dashboard.user.create');
		Route::post('/store', [DashboardUser::class, 'store'])->name('dashboard.user.store');
		Route::get('/edit/{id}', [DashboardUser::class, 'edit'])->name('dashboard.user.edit');
		Route::put('/update/{id}', [DashboardUser::class, 'update'])->name('dashboard.user.update');
		Route::delete('/delete/{id}', [DashboardUser::class, 'destroy'])->name('dashboard.user.destroy');
	});

	/**
	 * MANAGE ITEM TYPE
	 */
	Route::group(['prefix' => 'item-type'], function () {
		Route::get('/', [DashboardItemType::class, 'index'])->name('dashboard.item-type');
		Route::post('/store', [DashboardItemType::class, 'store'])->name('dashboard.item-type.store');
		Route::put('/update/{id}', [DashboardItemType::class, 'update'])->name('dashboard.item-type.update');
		Route::delete('/delete/{id}', [DashboardItemType::class, 'destroy'])->name('dashboard.item-type.destroy');
	});

	/**
	 * MANAGE ITEM
	 */
	Route::group(['prefix' => 'item'], function () {
		Route::get('/', [DashboardItem::class, 'index'])->name('dashboard.item');
		Route::get('/create', [DashboardItem::class, 'create'])->name('dashboard.item.create');
		Route::post('/store', [DashboardItem::class, 'store'])->name('dashboard.item.store');
		Route::get('/edit/{id}', [DashboardItem::class, 'edit'])->name('dashboard.item.edit');
		Route::put('/update/{id}', [DashboardItem::class, 'update'])->name('dashboard.item.update');
		Route::get('/{id}/show', [DashboardItem::class, 'show'])->name('dashboard.item.show');
		Route::delete('/delete/{id}', [DashboardItem::class, 'destroy'])->name('dashboard.item.destroy');
	});

	/**
	 * MANAGE ARTICLE TYPE
	 */
	Route::group(['prefix' => 'article-type'], function () {
		Route::get('/', [DashboardArticleType::class, 'index'])->name('dashboard.article-type');
		Route::post('/store', [DashboardArticleType::class, 'store'])->name('dashboard.article-type.store');
		Route::put('/update/{id}', [DashboardArticleType::class, 'update'])->name('dashboard.article-type.update');
		Route::delete('/delete/{id}', [DashboardArticleType::class, 'destroy'])->name('dashboard.article-type.destroy');
	});

	/**
	 * MANAGE ARTICLE
	 */
	Route::group(['prefix' => 'article'], function () {
		Route::get('/', [DashboardArticle::class, 'index'])->name('dashboard.article');
		Route::get('/create', [DashboardArticle::class, 'create'])->name('dashboard.article.create');
		Route::post('/store', [DashboardArticle::class, 'store'])->name('dashboard.article.store');
		Route::get('/edit/{id}', [DashboardArticle::class, 'edit'])->name('dashboard.article.edit');
		Route::put('/update/{id}', [DashboardArticle::class, 'update'])->name('dashboard.article.update');
		Route::delete('/delete/{id}', [DashboardArticle::class, 'destroy'])->name('dashboard.article.destroy');
	});

	/**
	 * MANAGE SUPPLIER
	 */
	Route::group(['prefix' => 'supplier'], function () {
		Route::get('/', [DashboardSuplier::class, 'index'])->name('dashboard.suplier');
		Route::post('/store', [DashboardSuplier::class, 'store'])->name('dashboard.suplier.store');
		Route::put('/update/{id}', [DashboardSuplier::class, 'update'])->name('dashboard.suplier.update');
		Route::delete('/delete/{id}', [DashboardSuplier::class, 'destroy'])->name('dashboard.suplier.destroy');
	});

	/**
	 * MANAGE LETTERS
	 */
	Route::group(['prefix' => 'letter'], function () {
		Route::get('/', [DashboardLetter::class, 'index'])->name('dashboard.letter');
		Route::post('/store', [DashboardLetter::class, 'store'])->name('dashboard.letter.store');
		Route::put('/update/{id}', [DashboardLetter::class, 'update'])->name('dashboard.letter.update');
		Route::delete('/delete/{id}', [DashboardLetter::class, 'destroy'])->name('dashboard.letter.destroy');
	});

	/**
	 * MANAGE COSTS
	 */
	Route::group(['prefix' => 'cost'], function () {
		Route::get('/', [DashboardCost::class, 'index'])->name('dashboard.cost');
		Route::post('/', [DashboardCost::class, 'store'])->name('dashboard.cost.store');
		Route::put('/{id}', [DashboardCost::class, 'update'])->name('dashboard.cost.update');
		Route::delete('/{id}', [DashboardCost::class, 'destroy'])->name('dashboard.cost.destroy');
	});

	/**
	 * MANAGE TRANSACTIONS
	 */
	Route::group(['prefix' => 'transaction'], function () {
		Route::get('/{suplierId}/buy', [DashboardTransaction::class, 'buying'])->name('dashboard.transaction.buying');
		Route::post('/{suplierId}/buy', [DashboardTransaction::class, 'restock'])->name('dashboard.transaction.restock');
		Route::get('/', [DashboardTransaction::class, 'selling'])->name('dashboard.transaction.selling');
		Route::post('/', [DashboardTransaction::class, 'sale'])->name('dashboard.transaction.sale');
	});

	/**
	 * MANAGE SELVE & BANNER
	 */
	Route::group(['prefix' => 'selve'], function () {
		Route::get('/', [DashboardSelve::class, 'index'])->name('dashboard.selve');
		Route::put('/update/{id}', [DashboardSelve::class, 'update'])->name('dashboard.selve.update');
		Route::post('/banner', [DashboardBanner::class, 'store'])->name('dashboard.banner.store');
		Route::delete('/banner/{id}', [DashboardBanner::class, 'destroy'])->name('dashboard.banner.destroy');
	});

	/**
	 * MANAGE TRANSACTIONS
	 */
	Route::group(['prefix' => 'transaction'], function () {
		Route::get('/{suplierId}/buy', [DashboardTransaction::class, 'buying'])->name('dashboard.transaction.buying');
		Route::post('/{suplierId}/buy', [DashboardTransaction::class, 'restock'])->name('dashboard.transaction.restock');
		Route::get('/', [DashboardTransaction::class, 'selling'])->name('dashboard.transaction.selling');
		Route::post('/', [DashboardTransaction::class, 'sale'])->name('dashboard.transaction.sale');
	});

	/**
	 * MANAGE Modal
	 */
	Route::group(['prefix' => 'modal'], function () {
		Route::get('/', [DashboardModal::class, 'index'])->name('dashboard.modal');
		Route::post('/store', [DashboardModal::class, 'store'])->name('dashboard.modal.store');
		Route::put('/update/{id}', [DashboardModal::class, 'update'])->name('dashboard.modal.update');
		Route::delete('/delete/{id}', [DashboardModal::class, 'destroy'])->name('dashboard.modal.destroy');
	});

	/**
	 * MANAGE TRANSACTIONS
	 * MANAGE REPORT
	 */
	Route::group(['prefix' => 'report'], function () {
		Route::get('/', [DashboardReport::class, 'index'])->name('dashboard.report');
	});

});

/**
 *
 *
 * BLOG PAGES
 *
 * ================================================================
 */

	/**
	 * HOME
	*/
	Route::get('/blog', [BlogHome::class, 'index'])->name('blog.home');

	/**
	 * TENTANG KAMI / SELVE
	 */
	Route::get('/blog/tentang-kami', [BlogSelve::class, 'index'])->name('blog.tentang-kami');

	/**
	 * ARTICLE
	 */
	Route::get('blog/article', [BlogArticle::class, 'index'])->name('articles');
	Route::get('blog/article/{slug_type}', [BlogArticle::class, 'getByType'])->name('article.byType');
	Route::get('blog/article/{slug_type}/{slug}', [BlogArticle::class, 'show'])->name('article');

	/**
	 * COMMENT ARTICLE
	 */
	Route::post('blog/article/comment', [BlogComment::class, 'store'])->name('comment.store');

	/**
	 * ITEM
	 */
	Route::get('blog/product', [BlogItem::class, 'index'])->name('products');
	Route::get('blog/product/{slug_type}', [BlogItem::class, 'getByType'])->name('product.byType');
	Route::get('blog/product/{slug_type}/{slug}', [BlogItem::class, 'show'])->name('product');
