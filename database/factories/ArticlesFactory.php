<?php

namespace Database\Factories;

use App\Models\Article;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArticlesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Article::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence(7),
            'slug' => Str::slug($this->faker->sentence(7)),
            'short_description' => $this->faker->paragraph(4),
            'long_description' => $this->faker->phoneNumber(),
            'path' => $this->faker->image(storage_path('app/public/article'), 670, 335, null, false),
            'user_id' => $this->faker->numberBetween(1, 8),
            'article_type_id' => $this->faker->numberBetween(1, 5)
        ];
    }
}
