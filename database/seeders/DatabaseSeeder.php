<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Faker\Factory;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create('id_ID');

        for ($i = 0; $i < 8; $i++) {
            User::create([
            	'name' => $faker->name,
            	'email' => $faker->email,
            	'phone' => $faker->phoneNumber,
                'nik' => $faker->numerify('###########'),
            	'password' => Hash::make('password')
            ]);
        }
    }
}
