<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create('id_ID');

        for ($i = 0; $i < 8; $i++) {
            User::create([
            	'name' => $faker->name,
            	'email' => $faker->email,
            	'phone' => $faker->phoneNumber,
                'nik' => $faker->numerify('###########'),
            	'password' => Hash::make('password')
            ]);
        }
    }
}
