<?php

namespace Database\Seeders;

use App\Models\Article;
use Faker\Factory;
use Illuminate\Database\Seeder;

class ArticlesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create('id_ID');

        for ($i = 0; $i < 8; $i++) {
            Article::create([
            	'title' => $faker->sentence,
                'slug' => makeSlug($faker->sentence),
                'short_description' => $faker->paragraph(4),
                'long_description' => $faker->paragraph(5),
                'path' => $faker->image(storage_path('app/public/article'), 670, 335, null, false),
                'user_id' => $faker->numberBetween(1, 8),
                'article_type_id' => $faker->numberBetween(1, 4)
            ]);
        }
    }
}
