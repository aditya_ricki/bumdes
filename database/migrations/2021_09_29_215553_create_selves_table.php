<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSelvesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('selves', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('description');
            $table->text('about_us');
            $table->text('motto');
            $table->text('alamat');
            $table->string('email');
            $table->string('phone');
            $table->string('ig');
            $table->string('facebook');
            $table->string('youtube');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('selves');
    }
}
