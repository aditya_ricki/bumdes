<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Selves extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    public function banners()
    {
        return $this->hasMany(Banner::class, 'selve_id');
    }
}
