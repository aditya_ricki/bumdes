<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cost extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $appends = [
    	'used_at_formated'
    ];

    public function getUsedAtFormatedAttribute()
    {
    	return date('m/d/Y H:i A', strtotime($this->attributes['used_at']));
    }
}
