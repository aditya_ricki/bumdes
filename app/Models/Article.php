<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];
    protected $appends = [
    	'short_description_formated',
    ];

    public function article_type()
    {
    	return $this->hasOne(ArticleType::class, 'id', 'article_type_id');
    }

    public function user()
    {
    	return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function getShortDescriptionFormatedAttribute()
	{
	    $shortDescription = strip_tags($this->attributes['short_description']);

	    return substr($shortDescription, 0, 50) . '...';
	}

    public function getDescriptionFormatedAttribute()
	{
	    $shortDescription = strip_tags($this->attributes['long_description']);

	    return substr($shortDescription, 0, 250) . '...';
	}

    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])
                        ->translatedFormat('l, d F Y');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class)->whereNull('parent_id');
    }
}
