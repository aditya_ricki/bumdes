<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    public function item_type()
    {
        return $this->hasOne(ItemType::class, 'id', 'item_type_id');
    }

    public function suplier()
    {
        return $this->hasOne(Suplier::class, 'id', 'item_type_id');
    }
}
