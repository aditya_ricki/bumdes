<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Letter extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    public function getShortContentFormatedAttribute()
    {
        $formatContent = strip_tags($this->attributes['content']);

        return substr($formatContent, 0, 50) . '...';
    }
}
