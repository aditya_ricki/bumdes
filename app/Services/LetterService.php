<?php

namespace App\Services;

use App\Models\Letter;
use Illuminate\Support\Facades\Log;

class LetterService
{
    public function getOne($id)
    {
        try{
            $result = Letter::findOrFail($id);

            return $result;
        }catch(\Throwable $th){
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function getAll($request)
    {
        try{
            $results = Letter::when($request->text_search, function ($query) use ($request) {
    			$query->where('sender', 'like', '%' . $request->text_search . '%')->orWhere('date', 'like', '%' . $request->text_search . '%');
    		})
    		->orderBy('id', 'desc')
    		->paginate($request->per_page ?: 10);
            
            return $results;
        }catch(\Throwable $th){
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function store($request)
    {
        try{
            $path = '';

            if ($request->file('file')) {
                $path = uploadFile($request->file('file'), 'letters');
            }

            $result = Letter::create([
                'sender' => $request->sender,
                'content' => $request->content,
                'date' => $request->date,
                'path' => $path,
            ]);

            return $result;
        }catch(\Throwable $th){
            dd($th->getMessage());
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function update($request, Letter $letter)
    {
        try{
            $path = $letter->path;

            if ($request->file('file')) {
                $path = uploadFile($request->file('file'), 'letters');
            }

            $result = $letter->update([
                'sender' => $request->sender,
                'content' => $request->content,
                'date' => $request->date,
                'path' => $path,
            ]);
            
            return $result;
        }catch(\Throwable $th){
            dd($th->getMessage());
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function destroy(Letter $letter)
 	{
 		try {
            deleteFile($letter->path);

 			$result = $letter->delete();

	   		return $result;
 		} catch (\Throwable $th) {
 			Log::error("Service error. " . $th->getMessage());
 			abort(500);
 		}
 	}
}