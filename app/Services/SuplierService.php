<?php

namespace App\Services;

use App\Models\Suplier;
use Illuminate\Support\Facades\Log;

class SuplierService
{
    public function getOne($id)
    {
        try{
            $result = Suplier::findOrFail($id);

            return $result;
        }catch(\Throwable $th){
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function getAll($request)
    {
        try{
            $results = Suplier::when($request->text_search, function ($query) use ($request) {
    			$query->where('name', 'like', '%' . $request->text_search . '%');
    		})
    		->orderBy('id', 'desc')
    		->paginate($request->per_page ?: 10);

            return $results;
        }catch(\Throwable $th){
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function getAllWithoutPaginate()
    {
        try{
            $results = Suplier::orderBy('created_at', 'desc')->get();

            return $results;
        }catch(\Throwable $th){
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function store($request)
    {
        try{
            $result = Suplier::create([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone
            ]);

            return $result;
        }catch(\Throwable $th){
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function update($request, Suplier $suplier)
    {
        try{
            $suplier->update([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone
            ]);

            return $suplier;
        }catch(\Trhowable $th){
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function destroy(Suplier $suplier)
 	{
 		try {
 			$result = $suplier->delete();

	   		return $result;
 		} catch (\Throwable $th) {
 			Log::error("Service error. " . $th->getMessage());
 			abort(500);
 		}
 	}
}