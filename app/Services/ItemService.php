<?php

namespace App\Services;

use App\Models\Item;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class ItemService
{
    public function getOne($id)
    {
        try{
            $result = Item::with('item_type', 'suplier')->findOrFail($id);

            return $result;
        }catch(\Throwable $th){
            Log::error('Service error ' . $th->getMessage());
            abort(500);
        }
    }

    public function getToHome($request)
    {
        try {
            $results = Item::with('item_type')
            ->when($request->text_search, function ($query) use ($request) {
    			$query->where('name', 'like', '%' . $request->text_search . '%');
    		})
            ->limit(8)
            ->get();

            return  $results;
        } catch (\Throwable $th) {
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function getBySlug($request)
    {
        try{
            $results = Item::whereHas('item_type', function ($query) use ($request){
                $query->where('slug', '=', $request->slug_type);
            })
            ->when($request->text_search, function ($query) use ($request) {
    			$query->where('name', 'like', '%' . $request->text_search . '%');
    		})
    		->orderBy('id', 'desc')
    		->paginate($request->per_page ?: 10);

            return $results;
        }catch(\Throwable $th){
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function getAll($request)
    {
        try{
            $results = Item::with('item_type')
            ->when($request->text_search, function ($query) use ($request) {
    			$query->where('name', 'like', '%' . $request->text_search . '%');
    		})
    		->orderBy('id', 'desc')
    		->paginate($request->per_page ?: 10);

            return $results;
        }catch(\Throwable $th){
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function getAllWithoutPaginate()
    {
        try{
            $results = Item::orderBy('id', 'desc')
            ->get();

            return $results;
        }catch(\Throwable $th){
            Log::error('Service error ' . $th->getMessage());
            abort(500);
        }
    }

    public function getBySupplierWithoutPaginate($supplierId)
    {
        try{
            $results = Item::where('suplier_id', $supplierId)
            ->orderBy('id', 'desc')
            ->get();

            return $results;
        }catch(\Throwable $th){
            Log::error('Service error ' . $th->getMessage());
            abort(500);
        }
    }

    public function show($request)
    {
        try{
            $result = Item::with('item_type')
            ->where('slug', '=', $request->slug)
            ->first();

            return $result;
        }catch(\Throwable $th){
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function store($request)
    {
        try{
            $path = '';

            if ($request->file('photo')) {
                $path = uploadFile($request->file('photo'), 'items');
            }

            $result = Item::create([
                'code' => $request->code,
                'name' => $request->name,
                'slug' => makeSlug($request->name),
                'short_description' => $request->short_description,
                'description' => $request->description,
                'capital_price' => $request->capital_price,
                'selling_price' => $request->selling_price,
                'stock' => $request->stock,
                'path' => $path,
                'item_type_id' => $request->item_type_id,
                'suplier_id' => $request->suplier_id,
            ]);

            return $result;
        }catch(\Throwable $th){
            dd($th->getMessage());
            Log::error('Service error ' . $th->getMessage());
            abort(500);
        }
    }

    public function update($request, Item $item)
    {
        try{
            $path = $item->path;

            if ($request->file('photo')) {
                $path = uploadFile($request->file('photo'), 'items');
            }

            $result = $item->update([
                'code' => $request->code,
                'name' => $request->name,
                'slug' => makeSlug($request->name),
                'short_description' => $request->short_description,
                'description' => $request->description,
                'capital_price' => $request->capital_price,
                'selling_price' => $request->selling_price,
                'stock' => $request->stock,
                'path' => $path,
                'item_type_id' => $request->item_type_id,
                'suplier_id' => $request->suplier_id,
            ]);

            return $result;
        }catch(\Throwable $th){
            dd($th->getMessage());
            Log::error('Service error ' . $th->getMessage());
            abort(500);
        }
    }

    public function destroy(Item $item)
 	{
 		try {
            deleteFile($item->path);

 			$result = $item->delete();

	   		return $result;
 		} catch (\Throwable $th) {
 			Log::error("Service error. " . $th->getMessage());
 			abort(500);
 		}
 	}
}