<?php

namespace App\Services;

use App\Models\Selves;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class SelveService
{
    public function getOne($id)
    {
        try{
            $result = Selves::findOrFail($id);

            return $result;
        }catch(\Throwable $th){
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public static function getData()
    {
        try{
            $results = Selves::with('banners')->first();

            return $results;
        }catch(\Throwable $th){
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function store($request)
    {
        try{
            DB::beginTransaction();

            $result = Selves::create([
                'title'       => $request->title,
                'description' => $request->description,
                'about_us'    => $request->about_us,
                'motto'       => $request->motto,
                'alamat'      => $request->alamat,
                'email'       => $request->email,
                'phone'       => $request->phone,
                'ig'          => $request->ig,
                'facebook'    => $request->facebook,
                'youtube'     => $request->youtube
            ]);

            DB::commit();

            return $result;

        }catch(\Throwable $th){
            DB::rollback();
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function update($request, Selves $selves)
    {
        try{
            DB::beginTransaction();
            
            $selves->update([
                'title'       => $request->title,
                'description' => $request->description,
                'about_us'    => $request->about_us,
                'motto'       => $request->motto,
                'alamat'      => $request->alamat,
                'email'       => $request->email,
                'phone'       => $request->phone,
                'ig'          => $request->ig,
                'facebook'    => $request->facebook,
                'youtube'     => $request->youtube
            ]);

            DB::commit();

            return $selves;
        }catch(\Throwable $th){
            DB::rollback();
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function destroy(Selves $Selves)
 	{
 		try {
 			DB::beginTransaction();

 			$result = $Selves->delete();

	   		DB::commit();

	   		return $result;
 		} catch (\Throwable $th) {
 			DB::rollback();
 			Log::error("Service error. " . $th->getMessage());
 			abort(500);
 		}
 	}
}