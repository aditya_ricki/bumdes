<?php

namespace App\Services;

use App\Models\Comment;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class CommentService
{
 	public function store($request)
 	{
 		try {
 			DB::beginTransaction();

	   		$result = Comment::create([
	       		'parent_id' => $request->parent_id,
                'name' => $request->name,
                'email' => $request->email,
                'comment' => $request->comment,
                'article_id' => $request->article_id,
	   		]);

	   		DB::commit();

	   		return $result;
 		} catch (\Throwable $th) {
 			DB::rollback();
 			Log::error("Service error. " . $th->getMessage());
 			abort(500);
 		}
 	}

 	public function destroy(Comment $comment)
 	{
 		try {
 			DB::beginTransaction();

 			$result = $comment->delete();

	   		DB::commit();

	   		return $result;
 		} catch (\Throwable $th) {
 			DB::rollback();
 			Log::error("Service error. " . $th->getMessage());
 			abort(500);
 		}
 	}
}
