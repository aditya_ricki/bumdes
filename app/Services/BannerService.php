<?php

namespace App\Services;

use App\Models\Banner;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class BannerService
{
    public function getOne($id)
    {
        try{
            $result = Banner::findOrFail($id);

            return $result;
        }catch(\Throwable $th){
            Log::error('Service error ' . $th->getMessage());
            abort(500);
        }
    }

    public static function getBanners($role)
    {
        try{
            $result = Banner::where('role', $role)
                        ->orderBy('id', 'asc')
                        ->get();

            return $result;
        }catch(\Throwable $th){
            Log::error('Service error ' . $th->getMessage());
            abort(500);
        }
    }

    public function store($request)
    {
        try{
            $path = '';

            if ($request->file('photo')) {
                $path = uploadFile($request->file('photo'), 'banners');
            }
            DB::beginTransaction();

            $result = Banner::create([
                'path' => $path,
                'role' => $request->role,
                'selve_id' => $request->selve_id,
            ]);

            DB::commit();

            return $result;
        }catch(\Throwable $th){
            DB::rollBack();
            Log::error('Service error ' . $th->getMessage());
            abort(500);
        }
    }

    public function destroy(Banner $banner)
 	{
 		try {
            deleteFile($banner->path);
            
 			$result = $banner->delete();

	   		return $result;
 		} catch (\Throwable $th) {
 			Log::error("Service error. " . $th->getMessage());
 			abort(500);
 		}
 	}
}