<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class UserService
{
	public function getOne($id)
	{
 		try {
 			$result = User::findOrFail($id);

	   		return $result;
 		} catch (\Throwable $th) {
 			DB::rollback();
 			Log::error("Service error. " . $th->getMessage());
 			abort(500);
 		}
	}

	public function getAllWithoutPaginate()
	{
		try {
			$result = User::get();

			return $result;
		} catch (\Throwable $th) {
			Log::error("Service error. " . $th->getMessage());
    		abort(500);
		}
	}

	public function getAll($request)
	{
    	try {
    		$results = User::when($request->text_search, function ($query) use ($request) {
    			$query->where('name', 'like', '%' . $request->text_search . '%');
    		})
    		->when($request->text_search, function ($query) use ($request) {
    			$query->orWhere('phone', 'like', '%' . $request->text_search . '%');
    		})
    		->when($request->text_search, function ($query) use ($request) {
    			$query->orWhere('email', 'like', '%' . $request->text_search . '%');
    		})
    		->orderBy('id', 'desc')
    		->paginate($request->per_page ?: 10);

    		return $results;
    	} catch (\Throwable $th) {
    		Log::error("Service error. " . $th->getMessage());
    		abort(500);
    	}
	}

 	public function store($request)
 	{
 		try {
			$path = '';

            if ($request->file('photo')) {
                $path = uploadFile($request->file('photo'), 'users');
            }

 			DB::beginTransaction();

	   		$result = User::create([
	       		'name' => $request->name,
				'nik' => $request->nik,
	       		'email' => $request->email,
	       		'phone' => $request->phone,
	       		'password' => makeHash($request->password),
				'path' => $path,
	   		]);

	   		DB::commit();

	   		return $result;
 		} catch (\Throwable $th) {
 			DB::rollback();
 			Log::error("Service error. " . $th->getMessage());
 			abort(500);
 		}
 	}

 	public function update($request, User $user)
 	{
 		try {
			$path = $user->path;

            if ($request->file('photo')) {
                $path = uploadFile($request->file('photo'), 'users');
            }
 			DB::beginTransaction();

 			$password = $user->password;

 			if ($request->password) {
 				$password = makeHash($request->password);
 			}

 			$user->update([
 				'name' => $request->name,
				 'nik' => $request->nik,
	       		'email' => $request->email,
	       		'phone' => $request->phone,
	       		'password' => $password,
				'path' => $path,
 			]);

	   		DB::commit();

	   		return $user;
 		} catch (\Throwable $th) {
 			DB::rollback();
 			Log::error("Service error. " . $th->getMessage());
 			abort(500);
 		}
 	}

 	public function destroy(User $user)
 	{
 		try {
 			DB::beginTransaction();

 			$result = $user->delete();

	   		DB::commit();

	   		return $result;
 		} catch (\Throwable $th) {
 			DB::rollback();
 			Log::error("Service error. " . $th->getMessage());
 			abort(500);
 		}
 	}
}
