<?php

namespace App\Services;

use App\Models\ItemType;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class ItemTypeService
{
	public function getOne($id)
	{
 		try {
 			$result = ItemType::findOrFail($id);

	   		return $result;
 		} catch (\Throwable $th) {
 			DB::rollback();
 			Log::error("Service error. " . $th->getMessage());
 			abort(500);
 		}
	}

	public function getAll($request)
	{
    	try {
    		$results = ItemType::when($request->text_search, function ($query) use ($request) {
    			$query->where('name', 'like', '%' . $request->text_search . '%');
    		})
    		->orderBy('id', 'desc')
    		->paginate($request->per_page ?: 10);

    		return $results;
    	} catch (\Throwable $th) {
    		Log::error("Service error. " . $th->getMessage());
    		abort(500);
    	}
	}

	public function getBySlug($slug)
    {
        try{
            $result = ItemType::where('slug', '=', $slug)->first();

            return $result;
        }catch(\Throwable $th){
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

	public function getAllWithoutPagination()
	{
		try {
			$result = ItemType::orderBy('created_at', 'desc')->get();

			return $result;
		} catch (\Throwable $th) {
			Log::error("Service error. " . $th->getMessage());
    		abort(500);
		}
	}

 	public function store($request)
 	{
 		try {
 			DB::beginTransaction();

	   		$result = ItemType::create([
	       		'name' => $request->name,
				'slug' => makeSlug($request->name),
                'short_description' => $request->short_description,
	   		]);

	   		DB::commit();

	   		return $result;
 		} catch (\Throwable $th) {
 			DB::rollback();
 			Log::error("Service error. " . $th->getMessage());
 			abort(500);
 		}
 	}

 	public function update($request, ItemType $itemType)
 	{
 		try {
 			DB::beginTransaction();

 			$itemType->update([
	       		'name' => $request->name,
				'slug' => makeSlug($request->name),
                'short_description' => $request->short_description,
 			]);

	   		DB::commit();

	   		return $itemType;
 		} catch (\Throwable $th) {
 			DB::rollback();
 			Log::error("Service error. " . $th->getMessage());
 			abort(500);
 		}
 	}

 	public function destroy(ItemType $itemType)
 	{
 		try {
 			DB::beginTransaction();

 			$result = $itemType->delete();

	   		DB::commit();

	   		return $result;
 		} catch (\Throwable $th) {
 			DB::rollback();
 			Log::error("Service error. " . $th->getMessage());
 			abort(500);
 		}
 	}
}
