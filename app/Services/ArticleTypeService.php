<?php

namespace App\Services;

use App\Models\ArticleType;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class ArticleTypeService
{
    public function getOne($id)
    {
        try{
            $result = ArticleType::findOrFail($id);

            return $result;
        }catch(\Throwable $th){
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function getAllWithoutPaginate()
    {
        try{
            $results = ArticleType::orderBy('created_at', 'desc')->get();

            return $results;
        }catch(\Throwable $th){
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function getBySlug($slug)
    {
        try{
            $result = ArticleType::where('slug', '=', $slug)->first();

            return $result;
        }catch(\Throwable $th){
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function getAll($request)
    {
        try{
            $results = ArticleType::when($request->text_search, function ($query) use ($request) {
    			$query->where('name', 'like', '%' . $request->text_search . '%');
    		})
    		->orderBy('id', 'desc')
    		->paginate($request->per_page ?: 10);

            return $results;
        }catch(\Throwable $th){
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function store($request)
    {
        try{
            DB::beginTransaction();

            $result = ArticleType::create([
                'name' => $request->name,
                'slug' => makeSlug($request->name)
            ]);

            DB::commit();

            return $result;

        }catch(\Throwable $th){
            DB::rollback();
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function update($request, ArticleType $ArticleType)
    {
        try{
            DB::beginTransaction();

            $ArticleType->update([
                'name' => $request->name,
                'slug' => makeSlug($request->name)
            ]);

            DB::commit();

            return $ArticleType;
        }catch(\Throwable $th){
            DB::rollback();
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function destroy(ArticleType $ArticleType)
 	{
 		try {
 			DB::beginTransaction();

 			$result = $ArticleType->delete();

	   		DB::commit();

	   		return $result;
 		} catch (\Throwable $th) {
 			DB::rollback();
 			Log::error("Service error. " . $th->getMessage());
 			abort(500);
 		}
 	}
}