<?php

namespace App\Services;

use App\Models\Article;
use Illuminate\Support\Facades\Log;

class ArticleService
{
    public function getOne($id)
    {
        try{
            $result = Article::with('user', 'article_type')->findOrFail($id);

            return $result;
        }catch(\Throwable $th){
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function getToHome($request)
    {
        try {
            $result = Article::with('user', 'article_type')
            ->when($request->text_search, function ($query) use ($request) {
    			$query->where('title', 'like', '%' . $request->text_search . '%');
    		})
            ->limit(4)->get();

            return $result;
        } catch (\Throwable $th) {
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function getBySlug($request)
    {
        try{
            $results = Article::whereHas('article_type', function ($query) use ($request){
                $query->where('slug', '=', $request->slug_type);
            })
            ->when($request->text_search, function ($query) use ($request) {
    			$query->where('title', 'like', '%' . $request->text_search . '%');
    		})
    		->orderBy('id', 'desc')
    		->paginate($request->per_page ?: 10);

            return $results;
        }catch(\Throwable $th){
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function show($request)
    {
        try{
            $result = Article::with('user', 'article_type', 'comments', 'comments.child')
            ->where('slug', '=', $request->slug)
            ->first();

            return $result;
        }catch(\Throwable $th){
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function getAll($request)
    {
        try{
            $results = Article::with('article_type')
            ->when($request->text_search, function ($query) use ($request) {
    			$query->where('title', 'like', '%' . $request->text_search . '%');
    		})
    		->orderBy('id', 'desc')
    		->paginate($request->per_page ?: 10);

            return $results;
        }catch(\Throwable $th){
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function store($request)
    {
        try{
            $path = '';

            if ($request->file('photo')) {
                $path = uploadFile($request->file('photo'), 'article');
            }

            $result = Article::create([
                'title' => $request->title,
                'slug' => makeSlug($request->title),
                'short_description' => $request->short_description,
                'long_description' => $request->long_description,
                'path' => $path,
                'article_type_id' => $request->article_type_id,
                'user_id' => $request->user_id,
            ]);

            return $result;
        }catch(\Throwable $th){
            dd($th->getMessage());
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function update($request, Article $article)
    {
        try{
            $path = $article->path;

            if ($request->file('photo')) {
                $path = uploadFile($request->file('photo'), 'article');
            }

            $article->update([
                'title' => $request->title,
                'slug' => makeSlug($request->title),
                'short_description' => $request->short_description,
                'long_description' => $request->long_description,
                'path' => $path,
                'article_type_id' => $request->article_type_id,
            ]);

            return $article;
        }catch(\Throwable $th){
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function destroy(Article $article)
 	{
 		try {
            deleteFile($article->path);

 			$result = $article->delete();

	   		return $result;
 		} catch (\Throwable $th) {
 			Log::error("Service error. " . $th->getMessage());
 			abort(500);
 		}
 	}
}