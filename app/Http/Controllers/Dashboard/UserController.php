<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\User;
use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Services\UserService;

class UserController extends Controller
{
    public function index(Request $request, UserService $service)
    {
    	try {
    		$results = $service->getAll($request);

    		return view('dashboard.user.index', compact('results'));
    	} catch (\Throwable $th) {
    		Log::error("Controller error. " . $th->getMessage());
    		abort(500);
    	}
    }

    public function create()
    {
        try {
            return view('dashboard.user.create');
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }

    public function store(StoreRequest $request, UserService $service)
    {
        try {
            $result = $service->store($request);

            return redirect()->route('dashboard.user')->with('success', 'Data created');
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }

    public function edit(UserService $service, $id)
    {
        try {
            $result = $service->getOne($id);

            return view('dashboard.user.edit', compact('result'));
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }

    public function update(UpdateRequest $request, UserService $service, $id)
    {
        try {
            $result = $service->update($request, $service->getOne($id));

            return redirect()->route('dashboard.user')->with('success', 'Data updated');
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }

    public function destroy(UserService $service, $id)
    {
        try {
            $result = $service->destroy($service->getOne($id));

            return redirect()->route('dashboard.user')->with('success', 'Data delete');
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }
}
