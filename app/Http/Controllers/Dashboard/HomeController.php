<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
	function __construct(Request $request)
	{
		$this->request = $request;
	}

    public function index()
    {
    	try {
    		return view('dashboard.home');
    	} catch (\Throwable $th) {
    		Log::error($th->getMessage());
    		abort(500);
    	}
    }
}
