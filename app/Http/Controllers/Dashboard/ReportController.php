<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Cost;
use Illuminate\Http\Request;
use App\Models\Transaction;
use Illuminate\Support\Facades\Log;

class ReportController extends Controller
{
	public function index(Request $request)
	{
        try{
        	$transactions = Transaction::with('transaction_details.item')
        	->when($request->from, function ($query) use($request) {
        		$query->where('created_at', '>=', $request->from);
        	})
        	->when($request->to, function ($query) use($request) {
        		$query->where('created_at', '<=', $request->to);
        	})
			// ->whereNotIn('trx_role', ['modal'])
        	->get();

        	$reports = [];

        	foreach ($transactions as $transaction) {
        		$itemReport = [];
        		$itemReport['id'] = $transaction->id;
				$itemReport['created_at'] = $transaction->created_at;
        		$itemReport['buyer'] = $transaction->buyer;
        		$itemReport['total_capital_price'] = 0;
        		$itemReport['total_selling_price'] = 0;
        		$itemReport['debit'] = $transaction->debit;
        		$itemReport['credit'] = $transaction->credit;
        		$itemReport['role'] = $transaction->trx_role;
	        	$itemReport['transaction_details'] = [];

        		foreach ($transaction->transaction_details as $transactionDetail) {
	        		$itemReport['total_capital_price'] += $transactionDetail->capital_price * $transactionDetail->quantity;
	        		$itemReport['total_selling_price'] += $transactionDetail->price * $transactionDetail->quantity;

	        		$itemTransactionDetail['id'] = $transactionDetail->id;
	        		$itemTransactionDetail['item'] = $transactionDetail->item->name;
	        		$itemTransactionDetail['quantity'] = $transactionDetail->quantity;
	        		$itemTransactionDetail['capital_price'] = $transactionDetail->capital_price;
	        		$itemTransactionDetail['selling_price'] = $transactionDetail->price;
	        		$itemTransactionDetail['subtotal_capital_price'] = $transactionDetail->capital_price * $transactionDetail->quantity;
	        		$itemTransactionDetail['subtotal_selling_price'] = $transactionDetail->price * $transactionDetail->quantity;

	        		array_push($itemReport['transaction_details'], $itemTransactionDetail);
        		}

        		array_push($reports, $itemReport);
        	}

			//get costs
        	$costs = Cost::when($request->from, function ($query) use($request) {
        		$query->where('used_at', '>=', $request->from);
        	})
        	->when($request->to, function ($query) use($request) {
        		$query->where('used_at', '<=', $request->to);
        	})->get();

			//laba/rugi

			$totalCost = 0;

			foreach ($costs as $cost) {
				$totalCost += $cost->cost;
			}

			$pendapatan = array_sum(array_column($reports, 'debit')) - array_sum(array_column($reports, 'credit'));

			$labaRugi = $pendapatan - $totalCost;

			$isLaba = true;

			if($labaRugi < 0){
				$isLaba = false;
			}

            return view('dashboard.report.index', [
				'costs' => $costs,
				'labaRugi' => $labaRugi,
				'isLaba' => $isLaba,
            	'reports' => $reports,
            	'debit' => array_sum(array_column($reports, 'debit')),
            	'credit' => array_sum(array_column($reports, 'credit')),
            ]);
        }catch(\Throwable $th){
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
	}

	public function incomeStatement(Request $request)
	{
		try {
			$transactions = Transaction::with('transaction_details.item')
        	->when($request->from, function ($query) use($request) {
        		$query->where('created_at', '>=', $request->from);
        	})
        	->when($request->to, function ($query) use($request) {
        		$query->where('created_at', '<=', $request->to);
        	})
        	->get();

			$pendapatan = 0;



		} catch (\Throwable $th) {
			Log::error("Controller error " . $th->getMessage());
			abort(500);
		}
	}
}
