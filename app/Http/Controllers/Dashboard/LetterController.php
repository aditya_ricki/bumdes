<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\Letter\StoreRequest;
use App\Http\Requests\Letter\UpdateRequest;
use App\Services\LetterService;

class LetterController extends Controller
{
    
    public function index(Request $request, LetterService $service)
    {
        try {
            $results = $service->getAll($request);
            
            return view('dashboard.letter.index', compact('results'));
        } catch (\Throwable $th) {
            Log::error("Controller error " . $th->getMessage());
            abort(500);
        }
    }

    public function store(StoreRequest $request, LetterService $service)
    {
        try {
            DB::beginTransaction();

            $service->store($request);

            DB::commit();

            return redirect()->route('dashboard.letter')->with('success', 'Data created');

        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error("Controller error " . $th->getMessage());
            abort(500);
        }
    }

    public function update(UpdateRequest $request, LetterService $service, $id)
    {
        try {
            DB::beginTransaction();

            $service->update($request, $service->getOne($id));

            DB::commit();

            return redirect()->route('dashboard.letter')->with('success', 'Data updated');

        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error("Controller error " . $th->getMessage());
            abort(500);
        }
    }

    public function destroy(LetterService $service, $id)
    {
        try {
            DB::beginTransaction();

            $service->destroy($service->getOne($id));

            DB::commit();

            return redirect()->route('dashboard.letter')->with('success', 'Data deleted');
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error("Controller error " . $th->getMessage());
            abort(500);
        }
    }
}
