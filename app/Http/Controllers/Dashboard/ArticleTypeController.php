<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\ArticleType\StoreRequest;
use App\Http\Requests\ArticleType\UpdateRequest;
use App\Services\ArticleTypeService;

class ArticleTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, ArticleTypeService $service)
    {
        try{
            $results = $service->getAll($request);

            return view('dashboard.article-type.index', compact('results'));
        }catch(\Throwable $th){
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request, ArticleTypeService $service)
    {
        try{
            $service->store($request);

            return redirect()->route('dashboard.article-type')->with('success', 'Data Created');
        }catch(\Throwable $th){
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ArticleType  $articleType
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, ArticleTypeService $service, $id)
    {
        try{
            $result = $service->update($request, $service->getOne($id));

            return redirect()->route('dashboard.article-type')->with('success', 'Data updated');
        }catch(\Throwable $th){
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ArticleType  $articleType
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArticleTypeService $service, $id)
    {
        try {
            $result = $service->destroy($service->getOne($id));

            return redirect()->route('dashboard.article-type')->with('success', 'Data delete');
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }
}
