<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cost;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class CostController extends Controller
{
    public function index(Request $request)
    {
        try{
            $results = Cost::when($request->text_search, function ($query) use ($request) {
    			$query->where('need', 'like', '%' . $request->text_search . '%');
    		})
    		->latest()
    		->paginate($request->per_page ?: 1);

            return view('dashboard.cost.index', compact('results'));
        }catch(\Throwable $th){
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    public function store(Request $request)
    {
        try{
            DB::beginTransaction();
            Cost::create([
                'need' => $request->need,
                'cost' => $request->cost,
                'used_at' => date('Y-m-d H:i:s', strtotime($request->used_at)),
            ]);

            DB::commit();
            return redirect()->back()->with('success', 'Data created');
        }catch(\Throwable $th){
            DB::rollback();
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    public function update(Request $request, $id)
    {
        try{
            DB::beginTransaction();
            $cost = Cost::findOrFail($id);

            $cost->update([
                'need' => $request->need,
                'cost' => $request->cost,
                'used_at' => date('Y-m-d H:i:s', strtotime($request->used_at)),
            ]);

            DB::commit();
            return redirect()->back()->with('success', 'Data updated');
        }catch(\Throwable $th){
            DB::rollback();
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }

    public function destroy($id)
    {
        try{
            DB::beginTransaction();
            $cost = Cost::findOrFail($id);

            $cost->delete();

            DB::commit();
            return redirect()->back()->with('success', 'Data deleted');
        }catch(\Throwable $th){
            DB::rollback();
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }
}
