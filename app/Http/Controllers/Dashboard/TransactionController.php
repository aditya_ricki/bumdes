<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Item;
use App\Models\Modal;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Services\ItemService;
use App\Models\TransactionDetail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class TransactionController extends Controller
{

    public function buying(Request $request, ItemService $itemService, $suplierId)
    {
        try{
        	$items = $itemService->getBySupplierWithoutPaginate($suplierId);

            return view('dashboard.transaction.buying', compact('items'));
        }catch(\Throwable $th){
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    public function restock(Request $request, $suplierId)
    {
		$request->validate([
			'id' => 'required|array',
			'name' => 'required|array',
			'capital_price' => 'required|array',
			'selling_price' => 'required|array',
			'stock' => 'required|array',
		]);

        try{
        	DB::beginTransaction();
            $modal = Modal::firstOrFail();

        	// create transaksi
        	$transaction = Transaction::create([
        		'suplier_id' => $suplierId,
        		'trx_code' => generateTrxCode(),
        		'trx_type' => 'CREDIT',
				'trx_role' => 'pembelian',
        		'status' => 'UNPAID',
        	]);

        	$total = 0;

        	// create transaksi detail & update item
        	foreach ($request->id as $key => $itemId) {
        		TransactionDetail::create([
        			'quantity' => $request->stock[$key],
        			'discount' => 0,
        			'price' => $request->selling_price[$key],
        			'capital_price' => $request->capital_price[$key],
        			'item_id' => $itemId,
        			'transaction_id' => $transaction->id,
        		]);

        		$item = Item::findOrFail($itemId);

        		$item->update([
        			'name' => $request->name[$key],
        			'capital_price' => $request->capital_price[$key],
        			'selling_price' => $request->selling_price[$key],
        			'stock' => $item->stock + $request->stock[$key],
        		]);

        		$total += $request->capital_price[$key];
        	}

        	// update transaksi
        	$transaction->update([
        		'credit' => $total,
        		'status' => 'PAID',
        	]);

            $modal->update([
                'modal' => $modal->modal - $total,
            ]);

        	DB::commit();

        	return redirect()->route('dashboard.suplier')->with('success', 'Berhasil restock produk');
        }catch(\Throwable $th){
        	DB::rollback();
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    public function selling(Request $request, ItemService $itemService)
    {
        try{
        	$items = $itemService->getAllWithoutPaginate();

            return view('dashboard.transaction.selling', compact('items'));
        }catch(\Throwable $th){
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    public function sale(Request $request)
    {
		$request->validate([
			'buyer' => 'required',
			'id' => 'required|array',
			'qty' => 'required|array',
		]);

        try{
        	DB::beginTransaction();
            $modal = Modal::firstOrFail();

        	// create transaksi
        	$transaction = Transaction::create([
        		'buyer' => $request->buyer,
        		'trx_code' => generateTrxCode(),
        		'trx_type' => 'DEBET',
				'trx_role' => 'penjualan',
        		'status' => 'UNPAID',
        	]);

        	$total = 0;

        	// create transaksi detail & update item
        	foreach ($request->id as $key => $itemId) {
        		$item = Item::findOrFail($itemId);

				if($item->stock - $request->qty[$key] < 0){
					DB::rollback();
					return redirect()->route('dashboard.transaction.selling')->with('error', 'Stok tidak cukup');
				}

				$item->update([
        			'stock' => $item->stock - $request->qty[$key],
        		]);

        		TransactionDetail::create([
        			'quantity' => $request->qty[$key],
        			'discount' => 0,
                    'price' => $item->selling_price,
        			'capital_price' => $item->capital_price,
        			'item_id' => $itemId,
        			'transaction_id' => $transaction->id,
        		]);


        		$total += $item->selling_price;
        	}

        	// update transaksi
        	$transaction->update([
        		'debit' => $total,
        		'status' => 'PAID',
        	]);

            $modal->update([
                'modal' => $modal->modal + $total,
            ]);

        	DB::commit();

        	return redirect()->route('dashboard.transaction.selling')->with('success', 'Pembelian berhasil');
        }catch(\Throwable $th){
        	DB::rollback();
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }
}
