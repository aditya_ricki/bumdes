<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Selve\UpdateRequest;
use App\Services\SelveService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SelveController extends Controller
{

    public function index(Request $request, SelveService $service)
    {
        try {
            $result = $service->getData();
            
            return view('dashboard.selve.index', compact('result'));
        } catch (\Throwable $th) {
            Log::error("Controller error " . $th->getMessage());
            abort(500);
        }
    }
    
    public function update(UpdateRequest $request, SelveService $service, $id)
    {
        try {
            $service->update($request, $service->getOne($id));

            return redirect()->route('dashboard.selve')->with('success', 'Data updated');

        } catch (\Throwable $th) {
            dd($th->getMessage());
            Log::error("Controller error " . $th->getMessage());
            abort(500);
        }
    }

}
