<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Suplier\StoreRequest;
use App\Http\Requests\Suplier\UpdateRequest;
use App\Services\SuplierService;
use Illuminate\Support\Facades\DB;

class SuplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, SuplierService $service)
    {
        try{
            $results = $service->getAll($request);

            return view('dashboard.suplier.index', compact('results'));
        }catch(\Throwable $th){
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request, SuplierService $service)
    {
        try{
            DB::beginTransaction();

            $result = $service->store($request);

            DB::commit();

            return redirect()->route('dashboard.suplier')->with('success', 'Data created');
        }catch(\Throwable $th){
            DB::rollback();
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Suplier  $suplier
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, SuplierService $service, $id)
    {
        try{
            DB::beginTransaction();

            $result = $service->update($request, $service->getOne($id));

            DB::commit();
            return redirect()->route('dashboard.suplier')->with('success', 'Data updated');
        }catch(\Throwable $th){
            DB::rollback();
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Suplier  $suplier
     * @return \Illuminate\Http\Response
     */
    public function destroy(SuplierService $service, $id)
    {
        try {
            DB::beginTransaction();

            $result = $service->destroy($service->getOne($id));

            DB::commit();
            return redirect()->route('dashboard.suplier')->with('success', 'Data delete');
        } catch (\Throwable $th) {
            DB::rollback();
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }
}
