<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Item\StoreRequest;
use App\Http\Requests\Item\UpdateRequest;
use App\Services\ItemService;
use App\Services\ItemTypeService;
use App\Services\SuplierService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, ItemService $service)
    {
        try {
            $results = $service->getAll($request);

            return view('dashboard.item.index', compact('results'));
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ItemTypeService $itemTypeService, SuplierService $suplierService)
    {
        try {
            $itemType = $itemTypeService->getAllWithoutPagination();
            $suplier = $suplierService->getAllWithoutPaginate();
            $code = 'PROD-' . str_shuffle(date('YmdHis'));

            return view('dashboard.item.create', compact('itemType', 'suplier', 'code'));
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request, ItemService $service)
    {
        try{
            DB::beginTransaction();

            $result = $service->store($request);

            DB::commit();
            return redirect()->route('dashboard.item')->with('success', 'Data Created');
        }catch(\Throwable $th){
            DB::rollback();
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ItemService $service , $id)
    {
        try {
            $result = $service->getOne($id);

            return view('dashboard.item.show', compact('result'));
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ItemTypeService $itemTypeService, SuplierService $suplierService, ItemService $service, $id)
    {
        try {
            $itemType = $itemTypeService->getAllWithoutPagination();
            $suplier = $suplierService->getAllWithoutPaginate();
            $result = $service->getOne($id);

            return view('dashboard.item.edit', compact('itemType', 'suplier', 'result'));

        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, ItemService $service, $id)
    {
        try {
            DB::beginTransaction();

            $result = $service->update($request, $service->getOne($id));

            DB::commit();

            return redirect()->route('dashboard.item')->with('success', 'Data updated');

        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ItemService $service, $id)
    {
        try {
            DB::beginTransaction();

            $result = $service->destroy($service->getOne($id));

            DB::commit();

            return redirect()->route('dashboard.item')->with('success', 'Data deleted.');
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }
}
