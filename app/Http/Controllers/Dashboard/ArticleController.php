<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Article\StoreRequest;
use App\Http\Requests\Article\UpdateRequest;
use App\Services\ArticleTypeService;
use App\Services\ArticleService;
use Illuminate\Support\Facades\Auth;

class ArticleController extends Controller
{
    public function index(Request $request, ArticleService $service)
    {
        try{
            $results = $service->getAll($request);

            return view('dashboard.article.index', compact('results'));
        }catch(\Throwable $th){
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    public function create(ArticleTypeService $articleTypeService)
    {
        try{
            $articleTypes = $articleTypeService->getAllWithoutPaginate();

            return view('dashboard.article.create', compact('articleTypes'));
        }catch(\Throwable $th){
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    public function store(StoreRequest $request, ArticleService $service)
    {
        try{
        	DB::beginTransaction();

        	$request->user_id = Auth::user()->id;
            $result = $service->store($request);

            DB::commit();
            return redirect()->route('dashboard.article')->with('success', 'Data Created');
        }catch(\Throwable $th){
        	DB::rollback();
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    public function edit(ArticleService $service, ArticleTypeService $articleTypeService, $id)
    {
        try{
            $articleTypes = $articleTypeService->getAllWithoutPaginate();
            $result = $service->getOne($id);

            return view('dashboard.article.edit', compact('result', 'articleTypes'));
        }catch(\Throwable $th){
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    public function update(UpdateRequest $request, ArticleService $service, $id)
    {
        try{
        	DB::beginTransaction();
            $result = $service->update($request, $service->getOne($id));

            DB::commit();
            return redirect()->route('dashboard.article')->with('success', 'Data updated');
        }catch(\Throwable $th){
        	DB::rollback();
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    public function show(ArticleService $service, $id)
    {
        try{
            $results = $service->getOne($id);

            return view('dashboard.article.show', compact('results'));
            
        }catch(\Throwable $th){
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    public function destroy(ArticleService $service, $id)
    {
        try {
            $result = $service->destroy($service->getOne($id));

            return redirect()->route('dashboard.article')->with('success', 'Data delete');
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }
}
