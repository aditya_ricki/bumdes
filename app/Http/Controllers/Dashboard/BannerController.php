<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Banner\StoreRequest;
use App\Services\BannerService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class BannerController extends Controller
{
    public function store(StoreRequest $request, BannerService $service)
    {
        try {
            $result = $service->store($request);

            return $result;
        } catch (\Throwable $th) {
            Log::error("Controller error " . $th->getMessage());
            abort(500);
        }
    }

    public function destroy(Request $request, BannerService $service, $id)
    {
        try {
            $result = $service->destroy($service->getOne($id));

            return $result;
        } catch (\Throwable $th) {
            Log::error("Controller error " . $th->getMessage());
            abort(500);
        }
    }
}
