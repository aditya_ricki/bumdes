<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Modal;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ModalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $results = Transaction::when($request->from, function ($query) use($request) {
        		$query->where('created_at', '>=', $request->from);
        	})
        	->when($request->to, function ($query) use($request) {
        		$query->where('created_at', '<=', $request->to);
        	})
            ->where('trx_role', 'modal')
        	->paginate($request->per_page ?: 10);

            $modal = Modal::first();

            return view('dashboard.modal.index', compact('results', 'modal'));
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'desc' => 'required',
            'nominal' => 'required|integer'
        ]);

        try {

            DB::beginTransaction();

            Transaction::create([
        		'trx_code' => generateTrxCode(),
        		'trx_type' => 'DEBET',
				'trx_role' => 'modal',
                'desc'  => $request->desc,
                'debit' => $request->nominal,
        		'status' => 'PAID',
            ]);

            //get modal
            $stateModal = Modal::firstOrCreate([
                'modal' => isset($stateModal) ? $stateModal->modal + $request->nominal : $request->nominal
            ]);

            DB::commit();

            return redirect()->route('dashboard.modal')->with('success', 'Modal ditambahkan');

        } catch (\Throwable $th) {

            DB::rollBack();

            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
