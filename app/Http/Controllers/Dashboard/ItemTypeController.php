<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\ItemType\StoreRequest;
use App\Http\Requests\ItemType\UpdateRequest;
use App\Services\ItemTypeService;

class ItemTypeController extends Controller
{
    public function index(Request $request, ItemTypeService $service)
    {
    	try {
    		$results = $service->getAll($request);

    		return view('dashboard.item-type.index', compact('results'));
    	} catch (\Throwable $th) {
    		Log::error("Controller error. " . $th->getMessage());
    		abort(500);
    	}
    }

    public function store(StoreRequest $request, ItemTypeService $service)
    {
        try {
            $result = $service->store($request);

            return redirect()->route('dashboard.item-type')->with('success', 'Data created');
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }

    public function update(UpdateRequest $request, ItemTypeService $service, $id)
    {
        try {
            $result = $service->update($request, $service->getOne($id));

            return redirect()->route('dashboard.item-type')->with('success', 'Data updated');
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }

    public function destroy(ItemTypeService $service, $id)
    {
        try {
            $result = $service->destroy($service->getOne($id));

            return redirect()->route('dashboard.item-type')->with('success', 'Data delete');
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }
}
