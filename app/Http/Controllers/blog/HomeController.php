<?php

namespace App\Http\Controllers\blog;

use App\Http\Controllers\Controller;
use App\Services\ArticleService;
use App\Services\ArticleTypeService;
use App\Services\ItemService;
use App\Services\ItemTypeService;
use App\Services\SelveService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $SelveService;

    function __construct(SelveService $SelveService)
    {
        $this->SelveService = $SelveService;
    }
    public function index(Request $request, ArticleService $ArticleService, UserService $UserService, ItemService $ItemService)
    {
        try {
            $selve = $this->SelveService->getData($request);
            $title = $selve->title;
            $meta_description = $selve->description;
            $articles = $ArticleService->getToHome($request);
            $users = $UserService->getAllWithoutPaginate();
            $items = $ItemService->getToHome($request);
            

            return view('blog.pages.home', compact('articles', 'users', 'items', 'title', 'meta_description'));
        } catch (\Throwable $th) {
            Log::error('Controller error', $th->getMessage());
            abort(500);
        }
    }
}
