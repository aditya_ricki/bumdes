<?php

namespace App\Http\Controllers\blog;

use App\Http\Controllers\Controller;
use App\Services\SelveService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SelveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $SelveService;

    function __construct(SelveService $SelveService)
    {
        $this->SelveService = $SelveService;
    }

    public function index(Request $request)
    {
        try {
            $selve = $this->SelveService->getData($request);
            $title = "Tentang Kami | " . $selve->title;
            $meta_description = $selve->description;

            return view('blog.pages.tentangkami', compact('selve', 'title', 'meta_description'));
        } catch (\Throwable $th) {
            Log::error("Controller error " . $th->getMessage());
            abort(500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
