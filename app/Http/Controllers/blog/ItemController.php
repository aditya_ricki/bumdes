<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use App\Services\ItemService;
use App\Services\ItemTypeService;
use App\Services\SelveService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $SelveService;

    function __construct(SelveService $SelveService)
    {
        $this->SelveService = $SelveService;
    }

    public function index(Request $request, ItemService $itemService, ItemTypeService $itemTypeService)
    {
        try {
            $title = "Belanja Kebutuhan Pokok di semarmara.com";
            $items = $itemService->getAll($request);
            $itemTypes = $itemTypeService->getAllWithoutPagination();
            $typeActive = null;
            $meta_description = $this->SelveService->getData($request)->description;
            
            return view('blog.pages.item.index', compact('items', 'itemTypes', 'typeActive', 'title', 'meta_description'));
        } catch (\Throwable $th) {
            Log::error("Controller error ". $th->getMessage());
            abort(500);
        }
    }

    public function getByType(Request $request, ItemService $itemService, ItemTypeService $itemTypeService)
    {
        try {
            $items = $itemService->getBySlug($request);
            $itemTypes = $itemTypeService->getAllWithoutPagination();
            $typeActive = $request->slug_type;
            $title = $itemTypeService->getBySlug($typeActive)->name;
            $meta_description = $itemTypeService->getBySlug($typeActive)->short_description;

            return view('blog.pages.item.index', compact('items', 'itemTypes', 'typeActive', 'title', 'meta_description'));
        } catch (\Throwable $th) {
            Log::error("Controller error ". $th->getMessage());
            abort(500);
        }
    }

    public function show(Request $request, ItemService $itemService, ItemTypeService $itemTypeService)
    {
        try {
            $item = $itemService->show($request);
            $itemTypes = $itemTypeService->getAllWithoutPagination();
            $typeActive = $request->slug_type;
            $title = $item->name;
            $meta_description = $item->short_description;
            
            return view('blog.pages.item.detail', compact('item', 'itemTypes', 'typeActive', 'title', 'meta_description'));
        } catch (\Throwable $th) {
            Log::error("Controller error ". $th->getMessage());
            abort(500);
        }
    }
}
