<?php

namespace App\Http\Controllers\blog;

use App\Http\Controllers\Controller;
use App\Services\ArticleService;
use App\Services\ArticleTypeService;
use App\Services\SelveService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $SelveService;

    function __construct(SelveService $SelveService)
    {
        $this->SelveService = $SelveService;
    }

    public function index(Request $request, ArticleService $articleService, ArticleTypeService $articleTypeService)
    {
        try {
            $selve = $this->SelveService->getData($request);
            $title = "Artikel";
            $articles = $articleService->getAll($request);
            $articleTypes = $articleTypeService->getAllWithoutPaginate();
            $typeActive = null;
            $meta_description = $selve->description;
            
            return view('blog.pages.article.index', compact('articles', 'articleTypes', 'typeActive', 'title', 'meta_description'));
        } catch (\Throwable $th) {
            Log::error("Controller error ". $th->getMessage());
            abort(500);
        }
    }

    public function getByType(Request $request, ArticleService $service, ArticleTypeService $articleTypeService)
    {
        try {
            $articles = $service->getBySlug($request);
            $articleTypes = $articleTypeService->getAllWithoutPaginate();
            $typeActive = $request->slug_type;
            $title = $articleTypeService->getBySlug($typeActive)->name;
            $meta_description = "Bumdes Nglumpang Mlarak Ponorogo";

            return view('blog.pages.article.index', compact('articles', 'articleTypes', 'typeActive', 'title', 'meta_description'));
        } catch (\Throwable $th) {
            Log::error("Controller error ". $th->getMessage());
            abort(500);
        }
    }

    public function show(Request $request, ArticleService $articleService, ArticleTypeService $articleTypeService)
    {
        try {
            $article = $articleService->show($request);
            $articleTypes = $articleTypeService->getAllWithoutPaginate();
            $typeActive = $request->slug_type;
            $title = $article->title;
            $meta_description = $article->short_description;
            
            return view('blog.pages.article.detail', compact('article', 'articleTypes', 'typeActive', 'title', 'meta_description'));
        } catch (\Throwable $th) {
            Log::error("Controller error ". $th->getMessage());
            abort(500);
        }
    }
}
