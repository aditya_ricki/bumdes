<?php

namespace App\Http\Requests\ArticleType;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        try {
            return [
                'name' => 'required',
            ];
        } catch (\Throwable $th) {
            Log::error("Request error. " . $th->getMessage());
            abort(500);
        }
    }
}
