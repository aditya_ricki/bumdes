<?php

namespace App\Http\Requests\Selve;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        try {
            return [
                'title'          => 'required',
                'description'    => 'required',
                'about_us'       => 'required',
                'motto'          => 'required',
                'alamat'         => 'required',
                'email'          => 'required|email:rfc,dns',
                'phone'          => 'required|min:10',
                'ig'             => 'required',
                'facebook'       => 'required',
                'youtube'        => 'required'
            ];
        } catch (\Throwable $th) {
            Log::error("Request error " . $th->getMessage());
            abort(500);
        }
    }
}
