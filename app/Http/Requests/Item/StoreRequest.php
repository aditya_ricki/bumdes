<?php

namespace App\Http\Requests\Item;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        try {
            return [
                'code' => 'required',
                'name' => 'required|string|max:255',
                'selling_price' => 'required',
                'capital_price' => 'required',
                'stock' => 'required',
                'description' => 'required',
                'short_description' => 'required',
                'photo' => 'required|file|mimes:jpg,png,jpeg|max:2000',
                'item_type_id' => 'required',
                'suplier_id' => 'required',
            ];
        } catch (\Throwable $th) {
            Log::error("Request error. " . $th->getMessage());
            abort(500);
        }
    }
}
