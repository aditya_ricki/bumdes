<?php

namespace App\Http\Requests\Article;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        try {
            return [
                'title' => 'required',
                'short_description' => 'required|string',
                'long_description' => 'required',
                'photo' => 'required|file|mimes:jpg,png,jpeg|max:2000',
                'article_type_id' => 'required',
            ];
        } catch (\Throwable $th) {
            Log::error("Request error. " . $th->getMessage());
            abort(500);
        }
    }
}
